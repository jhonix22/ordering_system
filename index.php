<?php
include 'admin/library/config.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Klarisa Pizzeria</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css" media="screen" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style-portfolio.css">
    <link rel="stylesheet" href="css/picto-foundry-food.css" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
	
	<link rel="stylesheet" href="src/facebox.css">
	<script src="src/facebox.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
		  $('a[rel*=facebox]').facebox({
			loadingImage : 'src/loading.gif',
			closeImage   : 'src/closelabel.png'
		  })
		})
	</script>
</head>

<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php#top"><div class="img-logo"></div></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav main-nav  clear navbar-right ">
                    <?php
						if(isset($_SESSION['name'])){
							
								$customer_id = $_SESSION['customer_id'];
								
									$query = "SELECT * FROM tbl_order_header WHERE customer_id = '$customer_id' AND transac_status = 'pending'";
									$result = mysql_query($query) or die(mysql_error());
									
									$count = mysql_num_rows($result);
									if($count != 1){
									$sqlInsert = "INSERT INTO tbl_order_header
											 (date_transac,customer_id)
											 VALUES
											 (NOW(),'$customer_id')";
									$res = mysql_query($sqlInsert) or die(mysql_error());
									}
					?>
					<div class="lbl-det">
						<div class="lbl-spn"></div>
						<span class="login-details">Welcome <?php echo $_SESSION['name'];?></span>
					</div>
					<li><a class="active color_animation" href="#top">WELCOME</a></li>
                    <li><a class="color_animation" href="#story">ABOUT</a></li>
                    <!--<li><a class="color_animation" href="#pricing">PRICING</a></li>-->
                    <li><a class="color_animation" href="#pricing">MENU</a></li>
                    <!--<li><a class="color_animation" href="#bread">BREAD</a></li>-->
                    <li><a class="color_animation" href="#featured">FEATURED</a></li>
                    <li><a class="color_animation" href="#reservation">RESERVATION</a></li>
                    <li><a class="color_animation" href="#contact">CONTACT</a></li>
                    <li><a class="color_animation" href="customer_logout.php?action=logout">LOGOUT</a></li>
					<?php
						}else{
					?>
					<li><a class="color_animation" href="#top">WELCOME</a></li>
                    <li><a class="color_animation" href="#story">ABOUT</a></li>
                    <!--<li><a class="color_animation" href="#pricing">PRICING</a></li>-->
                    <li><a class="color_animation" href="#pricing">MENU</a></li>
                    <!--<li><a class="color_animation" href="#bread">BREAD</a></li>-->
                    <li><a class="color_animation" href="#featured">FEATURED</a></li>
                    <li><a class="color_animation" href="#reservation">RESERVATION</a></li>
                    <li><a class="color_animation" href="#contact">CONTACT</a></li>
					<li><a class="color_animation" style="font-size:10px; width: 120px; margin-top: -20px;" href="customer_login.php">Login or Register here!</a></li>
					<?php
					}
					?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
     
    <div id="top" class="starter_container bg">
        <div class="follow_container">
            <div class="starter_follow">
                <h2 class="top-title"> Klarisa Pizzeria</h2>
                <h2 class="white second-title">" Best in the city "</h2>
                <hr>
            </div>
        </div>
        <div class="direction_container">
            <div class="direction">
                <a href="#story"></a>
            </div>
        </div>
    </div>


    <section id="story" class="description_content">
        <div class="limit">
            <div class="description_body margin-right">
                <h1>About us</h1>
                <div class="fa fa-cutlery fa-2x"></div>
                <p>Restaurant is a place for simplicity. Good food, good beer, and good service. Simple is the name of the game, and we’re good at finding it in all the right places, even in your dining experience. We’re a small group from Denver, Colorado who make simple food possible. Come join us and see what simplicity tastes like.</p>
            </div>
            <div class="picture_content margin-left">
                <ul class="image_box_story">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>
    </section>


    <section id ="pricing" class="background_content">
        <h1><span>Affordable</span> pricing</h1>
    </section>


    <section class="description_content">
        <div class="limit"> 
            <div class="container">
                <div class="row">
                    <div id="w">
                        <ul id="filter-list" class="clearfix">
						<?php
							$getCat = getCategory();
							foreach($getCat as $data){
						?>
                            <li class="filter" data-filter="<?php echo $data['category'];?>"><?php echo $data['category'];?></li>
						<?php
							}
							if(isset($_SESSION['name'])){
								$customer_id = $_SESSION['customer_id'];
								$getOrder = checkOrder($customer_id);
								foreach($getOrder as $check){
									$order_id = $check['order_id'];
								
								$sql = "SELECT * FROM tbl_order_details WHERE order_id = '$order_id'";
								$result = mysql_query($sql);

								$count = mysql_num_rows($result);
								}
								if($count < 1){
								?>
								<li><a onclick="alert('You Must Order first!')">Check Out</a></li>
								<?php
									}else{
								?>
								<li><a rel="facebox" href="options/checkout.php?order_id=<?php echo $customer_id;?>">Check Out</a></li>
								<?php
									}
							}
						?>
                        </ul><!-- @end #filter-list -->
                        <ul id="portfolio">
						<?php
							$getProd = getProduct();
							foreach($getProd as $value){
						?>
                            <li class="item <?php echo getAttribute("tbl_category","cat_id",$value['category'],"category");?>">
								<img src="admin/set/user_data/<?php echo $value['image'];?>" alt="Food" >
                                <h2 class="white"><?php echo $value['stock'];?><br/>
								<a rel="facebox" href="options/order.php?cat=<?php echo 
								getAttribute("tbl_category","cat_id",$value['category'],"category"
								);
								?>&id=<?php echo $value['stock_id'];?>&cus_id=<?php echo $customer_id;?>" id="btn-order">Order</a></h2>
                            </li>
						<?php
							}
						?>
                        </ul><!-- @end #portfolio -->
                    </div><!-- @end #w -->
                </div>
            </div>
        </div>  
    </section>

<!--
    <section id ="menu" class="background_content">
        <h1>Great <span>Place</span> to enjoy</h1>
    </section>


    <section class="description_content">
        <div class="limit">
            <div class="picture_content margin-right">
                <ul class="image_box_beer">
                    <li></li>
                </ul>
            </div>
      	    <div class="description_body margin-left">
                <h1>OUR MENU</h1>
                <div class="icon-beer fa-2x"></div>
                <p>Here at Restaurant we’re all about the love of beer. New and bold flavors enter our doors every week, and we can’t help but show them off. While we enjoy the classics, we’re always passionate about discovering something new, so stop by and experience our craft at its best.</p>
            </div>
        </div>
    </section>

   <section id="bread" class="background_content">
     <h1>Our Breakfast <span>Menu</span></h1>
   </section>


    <section class="description_content">
        <div class="limit">
            <div class="description_body margin-right">
                <h1>OUR BREAD</h1>
                <div class="icon-bread fa-2x"></div>
                <p>We love the smell of fresh baked bread. Each loaf is handmade at the crack of dawn, using only the simplest of ingredients to bring out smells and flavors that beckon the whole block. Stop by anytime and experience simplicity at its finest.</p>
            </div>
            <div class="picture_content margin-left">
                <ul class="image_box_story1">
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>
    </section>

-->
    <section id="featured" class="background_content">
        <h1>Our Featured Dishes <span>Menu</span></h1>
    </section>


    <section class="description_content">
        <div class="limit">
            <div class="description_body margin-right">
                <h1>Have a look to our dishes!</h1>
                <div class="icon-hotdog fa-2x"></div>
                <p>Each food is handmade at the crack of dawn, using only the simplest of ingredients to bring out smells and flavors that beckon the whole block. Stop by anytime and experience simplicity at its finest.</p>
            </div>
            <div class="picture_content margin-left">
                <ul class="image_box_story2">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="images/slider1.jpg"  alt="...">
                                <div class="carousel-caption">
                                    ...
                                </div>
                            </div>
                            <div class="item">
                                <img src="images/slider2.jpg" alt="...">
                                <div class="carousel-caption">
                                    ...
                                </div>
                            </div>
                            <div class="item">
                                <img src="images/slider3.jpg" alt="...">
                                <div class="carousel-caption">
                                    ...
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    </section>


    <section id="reservation" class="background_content">
        <h1>Reserve A Table!</h1>
    </section>


    <section class="description_content">
        <div class="limit">
            <div class="inner contact">
                <!-- Form Area -->
                <div class="contact-form">
                    <!-- Form -->
                    <form id="contact-us" method="post" action="reserve.php">
                        <!-- Left Inputs -->

                        <div class="container">
                            <div class="row">
                                <div class="col-lg-8 col-md-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                            <!-- Name -->
                                            <input type="text" name="first_name" id="first_name" required="required" class="form" placeholder="First Name" />
                                            <input type="text" name="last_name" id="last_name" required="required" class="form" placeholder="Last Name" />
                                            <input type="text" name="state" id="state" required="required" class="form" placeholder="State" />
                                            <input type="text" name="datepicker" id="datepicker" required="required" class="form" placeholder="Reservation Date" />
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                            <!-- Name -->
                                            <input type="text" name="phone" id="phone" required="required" class="form" placeholder="Phone" />
                                            <input type="text" name="guest" id="guest" required="required" class="form" placeholder="Guest Number" />
                                            <input type="email" name="email" id="email" required="required" class="form" placeholder="Email" />
                                            <input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
                                        </div>

                                        <div class="col-xs-12">
                                            <!-- Send Button -->
                                            <button type="submit" id="submit" name="submit" class=" form-btn form-btn1 semibold">Reserve</button> 
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-md-6 col-xs-12">
                                    <!-- Message -->
                                    <div class="right-text">

                                        <h2>Hours</h2><hr>
                                        <p>Mon to Fri: 7:30 AM - 11:30 AM</p>
                                        <p>Sat & Sun: 8:00 AM - 9:00 AM</p>
                                        <p>Lunch</p>

                                        <p>Mon to Fri: 12:00 PM - 5:00 PM</p>
                                        <p>Dinner</p>
                                        <p>Mon to Sat: 6:00 PM - 1:00 AM</p>
                                        <p>Sun: 5:30 PM - 12:00 AM</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Clear -->
                        <div class="clear"></div>
                    </form>

                </div><!-- End Contact Form Area -->
            </div><!-- End Inner -->
        </div>
    </section>


  

    <footer class="first_footer">
        <section class="limit">
            <div class="come_in">
                <span class="social_heading"></span>
                <span class="social_info"><a class="color_animation" href="https://www.google.com/maps/place/Venice,+FL/@27.114846,-82.4146291,13z/data=!3m1!4b1!4m2!3m1!1s0x88c34e48300b2dfb:0x52ac882236d6b362" target="_blank"></a></span>
            </div>
            <div class="connect">
			<div style="position:absolute; width: 227px; height: 443px; margin-left:-300px; margin-top: -150px; z-index:999;"><?php
   			$dlist = "SELECT * FROM up_files ";
			$dresult = mysql_query($dlist);
			if(mysql_num_rows($dresult) > 0)
				{
				while($ddata = mysql_fetch_assoc($dresult))
					{					
				 echo "<a type='submit' id='submit' name='submit' class=' form-btn form-btn1 semibold' 
				 style=' position:absolute; margin-left:23px; margin-top:300px; 
				 z-index:9999;' href='admin/upload/{$ddata['floc']}' title='download file' target='_blank' >Download</a>";
					}
				}
					?>
				<img src="images/mobile.png"/> 
				
            </div>
				<span class="social_heading">FOLLOW</span>
                <ul class="social_icons">
                    <li><a class="icon-twitter color_animation" href="#" target="_blank"></a></li>
                    <li><a class="icon-github color_animation" href="#" target="_blank"></a></li>
                    <li><a class="icon-linkedin color_animation" href="#" target="_blank"></a></li>
                    <li><a class="icon-mail color_animation" href="#"></a></li>
                </ul>
            </div>
            <div class="or_call">
                <span class="social_heading">OR DIAL</span>
                <span class="social_info"><a class="color_animation" href="tel:883-335-6524">(941) 883-335-6524</a></span>
            </div>
        </section>
    </footer>


    <div id="contact"  class="map">
        <!--<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d26081603.29442047!2d-95.677068!3d37.0625!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1421239704485" width="100%" height="400px" frameborder="0" style="border:0"></iframe>-->
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="post" action="contact.php">
                            <!-- Left Inputs -->
                            <div class="col-md-6 ">
                                <!-- Name -->
                                <input type="text" name="name" id="name" required="required" class="form" placeholder="Name" />
                                <!-- Email -->
                                <input type="email" name="email" id="email" required="required" class="form" placeholder="Email" />
                                <!-- Subject -->
                                <input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
                            </div><!-- End Left Inputs -->
                            <!-- Right Inputs -->
                            <div class="col-md-6">
                                <!-- Message -->
                                <textarea name="message" id="message" class="form textarea"  placeholder="Message"></textarea>
                            </div><!-- End Right Inputs -->
                            <!-- Bottom Submit -->
                            <div class="relative fullwidth col-xs-12">
                                <!-- Send Button -->
                                <button type="submit" id="submit" name="submit" class="form-btn semibold">Send Message</button> 
                            </div><!-- End Bottom Submit -->
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </div>
    </div>

    <footer class="sub_footer">
        <section class="limit">
            <div><p>&copy; Restaurant 2014</p></div>
            <div><p>BACK TO <a href="#top">TOP</a></p></div>
            <div><p>BUILT WITH CARE BY <a href="#" target="_blank">US</a></p></div>
        </section>
    </footer>


    <script type="text/javascript" src="js/jquery-1.10.2.min.js"> </script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="js/jquery.mixitup.min.js" ></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>      
    <script type="text/javascript" src="js/jquery.mixitup.min.js" ></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <script type="text/javascript">
        $(function() {
            $('a[href*=#]:not([href=#])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                      return false;
                    }
                }
            });
        });

    </script>


    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>

    <script type="text/javascript">
        $(function(){
            $('#portfolio').mixitup({
                targetSelector: '.item',
                transitionSpeed: 450
            });
        });
    </script>
</body>
</html>
