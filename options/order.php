    <link rel="stylesheet" href="css/order-style.css">
    <script type="text/javascript" src="js/script.js"></script>
<?php 
require_once '../admin/library/config.php';
if(!isset($_SESSION['name']))
    		{
    			echo '<br><div class="alert alert-warning"><h4>You must login first before you can order</h4><br>
    			<a href="customer_login.php" class="btn btn-default">Login Here</a>
    			</div>';
    			exit();
    		}else{
$prod_id = $_GET['id'];
$displayOrder = getProductOrder($prod_id);
$customer_id = $_GET['cus_id'];
$order_id = getOrderId($customer_id);
foreach($displayOrder as $data){
	$catAttribute = getAttribute("tbl_category","cat_id",$data['category'],"category");
	
foreach($order_id as $ordervalue){
	?>
		<style>
		.image-lbl img{
			width: 500px;
			height: 400px;
		}
		</style>
		<form action="options/process.php?pro=saveorder" method="POST">
	<?php
			if($catAttribute == 'pizza'){
		?>
			<input type="hidden" name="order_id" value="<?php echo $ordervalue['order_id'];?>"/>
			<input type="hidden" name="stock_id" value="<?php echo $data['stock_id'];?>"/>
			<div class="order-cnt">
			<div class="image-lbl">
				<img src="admin/set/user_data/<?php echo $data['image'];?>"/>
			</div>
				<div class="cnt-details">
					<span style="font-size: 24px;"><?php echo $data['stock'].' '.$catAttribute;?></span><hr/></br>
					<span class="lbl">Package:</span>
					<select style="padding: 10px;" name="package" onchange="packFunctions();" id="package" required>
					<option value="">Select a Package</option>
					<?php
						$package = getPackageList();
						foreach($package as $packData){
							?>
							<option value='<?php echo $packData['package_id'];?>'><?php echo $packData['desc'];?></option>
					<?php
						}
					?>
					</select><br/>
					<span style="position:absolute;">
						<h4>Package Prices</h4>
						<style>
							.tbl-price tr td{
								padding-right:20px;
								font-size: 18px;
							}
						</style>
						<table class="tbl-price">
							<tr><td>Regular</td><td align="">-</td><td align="right"><?php echo $data['price1'];?></td></tr>
							<tr><td>Family</td><td align="">-</td><td align="right"><?php echo $data['price2'];?></td></tr>
							<tr><td>Grande</td><td align="">-</td><td align="right"><?php echo $data['price3'];?></td></tr>
							<tr><td>Party</td><td align="">-</td><td align="right"><?php echo $data['price4'];?></td></tr>
							<tr><td>Supreme</td><td align="">-</td><td align="right"><?php echo $data['price5'];?></td></tr>
						</table>
					</span>
					<br/>
					<div class="cnt-form">
						<script>
							function packFunctions(){
								var x = document.getElementById("package").value;
								if(x=='1'){
									document.getElementById("input").innerHTML = "<span class='lbl-dd'>Price:</span><input type='hidden' name='package_id' value='"+x+"'/>";
									document.getElementById("input").innerHTML += "<input type='hidden' name='price1' id='value1' value='<?php echo $data['price1']?>'/>";
									document.getElementById("input").innerHTML += "<h2><?php echo $data['price1']?></h2>";
									document.getElementById("input").innerHTML += "<br/><span class='lbl-dd'>Qty:</span>";
									document.getElementById("input").innerHTML += "<input maxlength='2' id='value2' required onkeyup='myFunction();' onkeypress='return isNumberKey(event)' type='text' name='qty' placeholder='00'/><br/><br/><br/><br/>";
									document.getElementById("input").innerHTML += "<span class='lbl-dd'>Total:</span><br/><input type='hidden' id='total' name='total'/>";
									document.getElementById("input").innerHTML += "<sup>Php</sup><input maxlength='2' type='text' style='border:none;' disabled id='total2' value='00'/>";
								
								}else if(x=='2'){
									document.getElementById("input").innerHTML = "<span class='lbl-dd'>Price:</span><input type='hidden' name='package_id' value='"+x+"'/>";
									document.getElementById("input").innerHTML += "<input type='hidden' name='price2' id='value1' value='<?php echo $data['price2']?>'/>";
									document.getElementById("input").innerHTML += "<h2><?php echo $data['price2']?></h2>";
									document.getElementById("input").innerHTML += "<br/><span class='lbl-dd'>Qty:</span>";
									document.getElementById("input").innerHTML += "<input maxlength='2' id='value2' required onkeyup='myFunction();' onkeypress='return isNumberKey(event)' type='text' name='qty' placeholder='00'/><br/><br/><br/><br/>";
									document.getElementById("input").innerHTML += "<span class='lbl-dd'>Total:</span><br/><input type='hidden' id='total' name='total'/>";
									document.getElementById("input").innerHTML += "<sup>Php</sup><input maxlength='2' type='text' style='border:none;' disabled id='total2' value='00'/>";
								}else if(x=='3'){
									document.getElementById("input").innerHTML = "<span class='lbl-dd'>Price:</span><input type='hidden' name='package_id' value='"+x+"'/>";
									document.getElementById("input").innerHTML += "<input type='hidden' name='price3' id='value1' value='<?php echo $data['price3']?>'/>";
									document.getElementById("input").innerHTML += "<h2><?php echo $data['price3']?></h2>";
									document.getElementById("input").innerHTML += "<br/><span class='lbl-dd'>Qty:</span>";
									document.getElementById("input").innerHTML += "<input maxlength='2' id='value2' required onkeyup='myFunction();' onkeypress='return isNumberKey(event)' type='text' name='qty' placeholder='00'/><br/><br/><br/><br/>";
									document.getElementById("input").innerHTML += "<span class='lbl-dd'>Total:</span><br/><input type='hidden' id='total' name='total'/>";
									document.getElementById("input").innerHTML += "<sup>Php</sup><input maxlength='2' type='text' style='border:none;' disabled id='total2' value='00'/>";
								}else if(x=='4'){
									document.getElementById("input").innerHTML = "<span class='lbl-dd'>Price:</span><input type='hidden' name='package_id' value='"+x+"'/>";
									document.getElementById("input").innerHTML += "<input type='hidden' name='price4' id='value1' value='<?php echo $data['price4']?>'/>";
									document.getElementById("input").innerHTML += "<h2><?php echo $data['price4']?></h2>";
									document.getElementById("input").innerHTML += "<br/><span class='lbl-dd'>Qty:</span>";
									document.getElementById("input").innerHTML += "<input maxlength='2' id='value2' required onkeyup='myFunction();' onkeypress='return isNumberKey(event)' type='text' name='qty' placeholder='00'/><br/><br/><br/><br/>";
									document.getElementById("input").innerHTML += "<span class='lbl-dd'>Total:</span><br/><input type='hidden' id='total' name='total'/>";
									document.getElementById("input").innerHTML += "<sup>Php</sup><input maxlength='2' type='text' style='border:none;' disabled id='total2' value='00'/>";
								}else if(x=='5'){
									document.getElementById("input").innerHTML = "<span class='lbl-dd'>Price:</span><input type='hidden' name='package_id' value='"+x+"'/>";
									document.getElementById("input").innerHTML += "<input type='hidden' name='price5' id='value1' value='<?php echo $data['price5']?>'/>";
									document.getElementById("input").innerHTML += "<h2><?php echo $data['price5']?></h2>";
									document.getElementById("input").innerHTML += "<br/><span class='lbl-dd'>Qty:</span>";
									document.getElementById("input").innerHTML += "<input maxlength='2' id='value2' required onkeyup='myFunction();' onkeypress='return isNumberKey(event)' type='text' name='qty' placeholder='00'/><br/><br/><br/><br/>";
									document.getElementById("input").innerHTML += "<span class='lbl-dd'>Total:</span><br/><input type='hidden' id='total' name='total'/>";
									document.getElementById("input").innerHTML += "<sup>Php</sup><input maxlength='2' type='text' style='border:none;' disabled id='total2' value='00'/>";
								}else{
									document.getElementById("input").innerHTML = "";
								}
							}
						</script>
								<br/><package id="input"></package>
					</div>
				</div>
			</div>
			<?php
				}else{
			?>
					<div class="order-cnt">
						<div class="image-lbl">
							<img src="admin/set/user_data/<?php echo $data['image'];?>"/>
						</div>
						<div class="cnt-details">
							<input type="hidden" name="stock_id" value="<?php echo $data['stock_id'];?>"/>
							<input type="hidden" name="order_id" value="<?php echo $ordervalue['order_id'];?>"/>
							<input type="hidden" name="package_id" value="6"/>
								<span style="font-size: 24px;"><?php echo $data['stock'].' '.$catAttribute;?></span><hr/></br>
							<div style="padding-left:50px;">
								<br/><span class="lbl">Price</span>
								<h2><input type="hidden" name="price1" id="value1" value="<?php echo $data['price1']?>"><?php echo $data['price1']?></h2>
								<br/><span class='lbl-dd'>Qty:</span><input maxlength='2' id='value2' required onkeyup='myFunction();' onkeypress='return isNumberKey(event)' type='text' name='qty' placeholder='00'/><br/><br/><br/><br/>
								<span class='lbl-dd'>Total:</span><br/><input type='hidden' id='total' name='total'/>
								<sup>Php</sup><input maxlength='2' type='text' style='border:none;' disabled id='total2' value='00'/>
							</div>
						</div>
					</div>
			<?php
					}
			?>
			<div class="order-cnt">
					<input type="submit" id="btn-order" class="btn btn-default" value="Order"/>
			</div>
		</form>
		<?php
			}
		}
			}
		?>
		