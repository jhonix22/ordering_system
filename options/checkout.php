<style>
.tbl-chckout{
	width: 100%;
	background: #f8f8f8;
}
.tbl-chckout th{
	text-align: center;
	background: #435229;
	color: #fff;
	padding: 5px;
	border: 1px solid #637B38;
}
.tbl-chckout tr td{
	padding: 10px;
	border: 1px solid #000;
	text-align: center;
}
.tbl-chckout tr{
	background: rgb(255,255,255);
	background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(229,229,229,1) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(229,229,229,1)));
	background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 );

}
.tbl-chckout tr:hover{
	background: #fff;
}
.myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #caefab;
	-webkit-box-shadow:inset 0px 1px 0px 0px #caefab;
	box-shadow:inset 0px 1px 0px 0px #caefab;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77d42a), color-stop(1, #5cb811));
	background:-moz-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-webkit-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-o-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-ms-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:linear-gradient(to bottom, #77d42a 5%, #5cb811 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77d42a', endColorstr='#5cb811',GradientType=0);
	background-color:#77d42a;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #268a16;
	display:inline-block;
	cursor:pointer;
	color:#306108;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:10px 15px;
	text-decoration:none;
	text-shadow:0px 1px 0px #aade7c;
	float: right;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cb811), color-stop(1, #77d42a));
	background:-moz-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-webkit-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-o-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-ms-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:linear-gradient(to bottom, #5cb811 5%, #77d42a 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cb811', endColorstr='#77d42a',GradientType=0);
	background-color:#5cb811;
}
.myButton:active {
	position:relative;
	top:1px;
}

</style>
<?php
require_once '../admin/library/config.php';
?>
<script src="admin/js/jquery.js"></script>
<br/><br/><br/><b>Verify and Check Out Your Order</b><br/><br/>
	<form method="POST" action="options/process.php?pro=checkout">
		<table class="tbl-chckout">
			<th>Image</th><th>Name</th><th>Category</th><th>Package</th><th>Quantity</th><th>Price</th><th>Total Amount</th><th>Action</th>
			<?php
				$customer_id = $_GET['order_id'];
				$orderid = getOrderId($customer_id);
				foreach($orderid as $orderattribute){
					$order_id = $orderattribute['order_id'];
					$orderlist = getOrderList($order_id);
					foreach($orderlist as $value){
						$order_detail_id = $value['order_detail_id'];
						$id = $value['stock_id'];
						$getProdList = getProductOrder($id);
						foreach($getProdList as $displayValue){
						?>
						<tr class="show-del">
							<td><img src="admin/set/user_data/<?php echo $displayValue['image'];?>" height="40px" width="40px"></td>
							<td><?php echo $displayValue['stock'];?></td>
							<td><?php echo getAttribute("tbl_category","cat_id",$displayValue['category'],"category");?></td>
							<td>
								<?php
								$package_id = $value['package_id'];
								$package = getPackageOrder($package_id);
								foreach($package as $packageData){
									echo $packageData['desc'];
								}
								?>
							</td>
							<td><?php echo $value['qty'];?></td>
							<td>Php <?php echo number_format($value['price']);?></td>
							<td>Php <?php echo number_format($value['amount']);?></td>
							<td><a href="#" id="<?php echo $order_detail_id; ?>" class="delete">Remove</a></td>
						</tr>
						<?php
						error_reporting(0);
						$sum += $value['amount'];
						}
					}
					?>
						<script>
							$(document).ready(function(){
							setInterval(function(){
								$("#show").load('options/showResult.php?id=<?php echo $value['order_id']?>')
							}, 1000);
							});
						</script>
						<?php
				}
			?>
			<tr style="background:#fff;">
				<td colspan="6" style="text-align:right;"><b>Total: </b></td><td style="text-align:left; font-size: 25px;" colspan="2" id="show">
				
			</tr>
		</table>
		<input type="hidden" value="<?php echo $order_id;?>" name="order_id"/><br/>
		<input type="submit" value="Check Out" class="myButton">
	</form>
	<script type="text/javascript">
		$(function() {
		$(".delete").click(function(){
		var element = $(this);
		var del_id = element.attr("id");
		var info = 'id=' + del_id;
		if(confirm("Are you sure you want to delete this?"))
		{
		 $.ajax({
		   type: "POST",
		   url: "options/deleteOrderList.php",
		   data: info,
		   success: function(){
		 }
		});
		  $(this).parents(".show-del").animate({ backgroundColor: "#003" }, "slow")
		  .animate({ opacity: "hide" }, "slow");
		 }
		return false;
		});
		});
	</script>