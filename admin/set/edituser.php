<?php
require_once '../library/config.php';
?>
<style>
form input[type="text"]{
	padding: 10px;
}
form span{
	color: green;
	position: absolute;
	font-style: italic;
	font-weight: bold;
	margin-top: -20px;
}
form select{
	padding: 10px;
}
.myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #caefab;
	-webkit-box-shadow:inset 0px 1px 0px 0px #caefab;
	box-shadow:inset 0px 1px 0px 0px #caefab;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77d42a), color-stop(1, #5cb811));
	background:-moz-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-webkit-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-o-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-ms-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:linear-gradient(to bottom, #77d42a 5%, #5cb811 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77d42a', endColorstr='#5cb811',GradientType=0);
	background-color:#77d42a;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #268a16;
	display:inline-block;
	cursor:pointer;
	color:#306108;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:10px 15px;
	text-decoration:none;
	text-shadow:0px 1px 0px #aade7c;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cb811), color-stop(1, #77d42a));
	background:-moz-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-webkit-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-o-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-ms-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:linear-gradient(to bottom, #5cb811 5%, #77d42a 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cb811', endColorstr='#77d42a',GradientType=0);
	background-color:#5cb811;
}
.myButton:active {
	position:relative;
	top:1px;
}
.cnt-edit{
	margin-left: auto;
	margin-right: auto;
	width: 585px;
}
h3{
	text-align: center;
}
</style>
<br/><br/>
<div class="cnt-edit">
<h3>Edit User</h3><br/>
<form method="POST" action="set/editProcess.php?action=editUser">
<?php
$id = $_GET['id'];
$getDetails = getUserDetails($id);
foreach($getDetails as $value){
?>
	<input type="hidden" name="id" value="<?php echo $value['user_id'];?>"/>
	<span>Name</span><input type="text" name="username" value="<?php echo $value['username'];?>">
	<span>Password</span><input type="text" name="password" value="<?php echo $value['password'];?>">
<?php
}
?>
<input type="submit" value="Update!" class="myButton">
</form>
</div>
<br/><br/><br/><br/>