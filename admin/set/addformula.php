<?php
	include '../library/config.php';
?>
<div class="pop-style">
<h2>Add Formulation</h2>
	<form action="set/process.php?action=addFormulation" method="POST">
		Formulation Code:<br/>
		<input type="text" name="code" id="code" required="required" class="form" placeholder="Formulation Code" />
		<br/><br/>
		Finished Product: <br/>
		<?php
			echo getFPProduct(NULL,"stock_id");
		?>
		<br/><br/>
		Date:<br/>
		<input type="date" name="date" id="date" required="required" class="form" placeholder="Transaction Date" />
		<br/><br/>
		Category: <br/>
		<?php
			echo getFPCategory(NULL,"category_id");
		?>
		<br/><br/>
		
		<div id="po_details" style="display:inline-block;float:right;margin-top:-326px;">
			<h3>Formulation Ingredients</h3>
			<?php
			echo 'Raw Product: <br/>'.getRPProduct(NULL,"raw_id").'<br/><br/>';
			echo 'Quantity: <br/><input type="number" name="quantity" id="quantity" class="form" required placeholder="qty">&nbsp;&nbsp;
					<input type="button" id="button_add" value="Add"><br/><br/>';
			echo '<input type="hidden" name="temp_po_id" id="temp_po_id">';

			echo '<div id="list">
					<table border="0" width="100%" id="product_list">
					<tr>
						<th>Ingredients</th>
					</tr>';
			echo '<tr bgcolor="#C0C0C0">
						<td>Stock</td>
						<td>Quantity</td>
						<td>Amount</td>
				  </tr>';
			echo '</table>
				  </div>';
			?>
		</div>
		<br/><br/>
		<input type="submit" value="Save"/>
	</form>
</div>
<script>
	jQuery('#button_add').click(function(){
		var code = jQuery('#code').val();
		var date = jQuery('#date').val();
		var stock_id = jQuery('#stock_id').val();
		var category_id = jQuery('#category_id').val();

		if((date == "") || (code == "") || (stock_id == "") || (category_id == "")){
			alert("All fields are Required.");
		}else{
			var raw_id = jQuery('#raw_id').val();
			var qty = jQuery('#quantity').val();
			var temp_po_id = jQuery('#temp_po_id').val();

			if((raw_id == "") || (qty == "")){
				alert("Select Product or supply a quantity.");
			}else{
				jQuery.post("set/process.php?action=addTempF",
					{
						temp_po_id:temp_po_id,
						raw_id:raw_id,
						qty:qty,
						category_id: category_id,
						stock_id: stock_id,
						date: date,
						code: code
					},
				function(e){
					if(e=="Not Enough Inventory Balance."){
						alert(e);
					}else{
						jQuery('#temp_po_id').val(e);
						jQuery.post("set/process.php?action=getTempF",{temp_po_id:e},
						function(a){
							jQuery('#list').html(a);
						});
					}
				});
			}
		}
	});
</script>