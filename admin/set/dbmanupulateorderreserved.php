<link rel="stylesheet" href="src/facebox.css">
<script src="src/facebox.js"></script>
<script src="js/script.js"></script>
<?php
include('../paging/db.php');
include('../library/config.php');

 if(isset($_REQUEST['actionfunction']) && $_REQUEST['actionfunction']!=''){
$actionfunction = $_REQUEST['actionfunction'];
  
   call_user_func($actionfunction,$_REQUEST,$con,$limit,$adjacent);
}
function showData($data,$con,$limit,$adjacent) {
  $page = $data['page'];
   if($page==1){
   $start = 0;  
  }
  else{
  $start = ($page-1)*$limit;
  }
  $sql = "select * from tbl_order_header order by date_transac";
  $rows  = $con->query($sql);
  $rows  = $rows->num_rows;
  
  $sql = "select * from tbl_order_header order by date_transac desc limit $start,$limit";
  
  $data = $con->query($sql);
  $str='<table><tr class="head" align="center"><td>Order Id</td><td>Customer</td><td>Remarks</td><td>Date Reserved</td><td>Status</td>
		<td>Discount</td><td>Transaction Status</td><td>Action</td></tr>';
  if($data->num_rows>0){
   while( $row = $data->fetch_array(MYSQLI_ASSOC)){
	   $id = $row['customer_id'];
	   $getCus = getCustomerDetailsManupulate($id);
	   foreach($getCus as $details){
			   if($row['status'] == 'S' && $row['transac_status'] == 'pending'){
					$str.="<tr align='center'><td>".$row['order_id']."</td>
					<td><a rel='facebox' href='set/vieworderslist.php?id=".$row['order_id']."'>".$details['name']."</a></td>
					<td>".$row['remarks']."</td><td>".$row['date_transac']."</td>
					<td>".$row['status']."</td><td>".$row['discount']."</td><td>".$row['transac_status']."</td>
					<td><a href='set/process.php?action=confirmOrder&id=".$row['order_id']."'>Confirm</a> | 
					<a href='set/process.php?action=cancelOrder&id=".$row['order_id']."'>Cancel</a></td></tr>";
				}else if($row['status'] == 'C'){
					$str.="<tr align='center' style='background: orange;'><td>".$row['order_id']."</td>
					<td><a rel='facebox' href='set/vieworderslist.php?id=".$row['order_id']."'>".$details['name']."</a></td><td>".$row['remarks']."</td>
					<td>".$row['date_transac']."</td><td>".$row['status']."</td><td>".$row['discount']."</td><td>".$row['transac_status']."</td>
					<td>Cancelled</td></tr>";
				}else if($row['status'] == 'D'){
					$str.="<tr align='center' style='background: #0E9100;'><td>".$row['order_id']."</td>
					<td><a rel='facebox' href='set/vieworderslist.php?id=".$row['order_id']."'>".$details['name']."</a></td><td>".$row['remarks']."</td>
					<td>".$row['date_transac']."</td><td>".$row['status']."</td><td>".$row['discount']."</td><td>".$row['transac_status']."</td>
					<td>Finished</td></tr>";
				}else if($row['transac_status'] == 'reserved'){
					$str.="<tr align='center' style='background: #A3F3F0;'><td>".$row['order_id']."</td>
					<td><a rel='facebox' href='set/vieworderslist.php?id=".$row['order_id']."'>".$details['name']."</a></td><td>".$row['remarks']."</td>
					<td>".$row['date_transac']."</td><td>".$row['status']."</td><td>".$row['discount']."</td><td>".$row['transac_status']."</td>
					<td><a href='set/process.php?action=finished&id=".$row['order_id']."'>Update</a> | 
					<a href='set/process.php?action=cancelOrder&id=".$row['order_id']."'>Cancel</a></td></tr>";
				}
		   }
		}
   }else{
    $str .= "<td colspan='5'>No Data Available</td>";
   }
   $str.='</table>';
   
echo $str; 
pagination($limit,$adjacent,$rows,$page);  
}
function pagination($limit,$adjacents,$rows,$page){	
	$pagination='';
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$prev_='';
	$first='';
	$lastpage = ceil($rows/$limit);	
	$next_='';
	$last='';
	if($lastpage > 1)
	{	
		
		//previous button
		if ($page > 1) 
			$prev_.= "<a class='page-numbers' href=\"?page=$prev\">previous</a>";
		else{
			//$pagination.= "<span class=\"disabled\">previous</span>";	
			}
		
		//pages	
		if ($lastpage < 5 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
		$first='';
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a class='page-numbers' href=\"?page=$counter\">$counter</a>";					
			}
			$last='';
		}
		elseif($lastpage > 3 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			$first='';
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a class='page-numbers' href=\"?page=$counter\">$counter</a>";					
				}
			$last.= "<a class='page-numbers' href=\"?page=$lastpage\">Last</a>";			
			}
			
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
		       $first.= "<a class='page-numbers' href=\"?page=1\">First</a>";	
			for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a class='page-numbers' href=\"?page=$counter\">$counter</a>";					
				}
				$last.= "<a class='page-numbers' href=\"?page=$lastpage\">Last</a>";			
			}
			//close to end; only hide early pages
			else
			{
			    $first.= "<a class='page-numbers' href=\"?page=1\">First</a>";	
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a class='page-numbers' href=\"?page=$counter\">$counter</a>";					
				}
				$last='';
			}
            
			}
		if ($page < $counter - 1) 
			$next_.= "<a class='page-numbers' href=\"?page=$next\">next</a>";
		else{
			//$pagination.= "<span class=\"disabled\">next</span>";
			}
		$pagination = "<div class=\"pagination\">".$first.$prev_.$pagination.$next_.$last;
		//next button
		
		$pagination.= "</div>\n";		
	}

	echo $pagination;  
}
?>  