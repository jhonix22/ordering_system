<link rel="stylesheet" type="text/css" media="screen" href="css/modal.css" />
<div class="cnt">
	<nav>
		<ul>
			<li><a href="index.php?page=supplier">View Supplier</a></li>
			<li><a href="#" data-modal-open="modal-1">Add Supplier</a></li>
		</ul>
	</nav>
	<div class="body-content">
		<div id="text">Supplier</div>
		<div id="pagination" cellspacing="0">
		</div>
		<script type="text/javascript" src="paging/scriptsupplier.js"></script>
	</div>
</div>
<div id="modal-1" class="mb-modal">
<div class="close-modal">&#215;</div>
<div class="pop-style">
<h2>Add a Supplier</h2>
	<form action="set/process.php?action=addsup" method="POST">
		<input type="text" name="name" placeholder="Name of Supplier" required/>
		<input type="submit" value="Save"/>
	</form>
</div>
</div>
<script src="js/modal.min.js"></script>