<link rel="stylesheet" type="text/css" media="screen" href="css/modal.css" />
<div class="cnt">
	<nav>
		<ul>
			<li><a href="index.php?page=customer">Customers</a></li>
			<li><a href="#" data-modal-open="modal-1">Add Customer</a></li>
		</ul>
	</nav>
	<div class="body-content">
		<div id="text">Customer</div>
		<div id="pagination" cellspacing="0">
		</div>
		<script type="text/javascript" src="paging/scriptcustomer.js"></script>
	</div>
</div>
<div id="modal-1" class="mb-modal">
<div class="close-modal">&#215;</div>
<div class="pop-style">
<h2>Add a Customer</h2>
	<form action="set/process.php?action=addcus" method="POST">
		<input type="text" name="name" placeholder="Name of Customer" required/>
		<input type="date" name="bday" title="Birthday" required/>
		<input type="submit" value="Save"/>
	</form>
</div>
</div>
<script src="js/modal.min.js"></script>