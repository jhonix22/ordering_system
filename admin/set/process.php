<?php
include '../library/config.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'addsup': newSupplier();
	break;
	case 'editsup': editSupplier();
	break;
	case 'addcus': newCustomer();
	break;
	case 'editcus': editCustomer();
	break;
	case 'addemp': newEmployee();
	break;
	case 'editemp': editEmployee();
	break;
	case 'adduser': newUser();
	break;
	case 'edituser': editUser();
	break;
	case 'addproduct': addProduct();
	break;
	case 'addpo': addPO();
	break;
	case 'addTempstock': addTempStock();
	break;
	case 'getTempStocks': getTempStocks();
	break;
	case 'cancelPO': cancelPO();
	break;
	case 'cancelRR': cancelRR();
	break;
	case 'getPOTableForRR': getPOTableForRR();
	break;
	case 'addrr': addRR();
	break;
	case 'deleteProduct': deleteProduct();
	break;

	case 'addTempF': addTempF();
	break;
	case 'getTempF': getTempF();
	break;
	case 'addFormulation': addFormulation();
	break;

	case 'checkCureentStocks': checkCureentStocks();
	break;
	case 'addproduction': addproduction();
	break;
	
	case 'checkout': checkoutPOS();
	break;
	case 'confirm': checkoutTableReserved();
	break;
	case 'cancelTable': cancelTableReserved();
	break;
	case 'cancelOrder': cancelOrderReserved();
	break;
	case 'confirmOrder': confirmOrderReserved();
	break;
	case 'finished': finishedOrderReserved();
	break;


	default : header("Location: ../index.php");
}
//--------------Supplier-------------------------------------
function newSupplier(){
	$name = $_POST['name'];
	
	$query = "SELECT * FROM tbl_supplier WHERE name = '$name'";
	$result = mysql_query($query) or die(mysql_error());
	
	$count = mysql_num_rows($result);
	if($count == 1){
		$_SESSION['memExist']['type'] = 'success';
		$_SESSION['memExist']['message'] = 'Record Already Exist';
		
		header("Location: ../index.php?page=supplier");
	}else{
	$query = "INSERT INTO tbl_supplier SET
			name = '$name', date_added = NOW(), status = 'active'";
	$result = mysql_query($query) or die(mysql_error());
	
	$_SESSION['create']['type'] = 'success';
	$_SESSION['create']['message'] = 'New Record Added..';
	
	header("Location: ../index.php?page=supplier");
	}
	exit;
}

function checkoutPOS(){
	
	$id = $_POST['id'];

	$sql = "UPDATE tbl_pos_header SET status = 'D' WHERE pos_id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	echo "<script>alert('Successfully Updated!!');</script>";
	echo "<script>document.location.href = '../index.php?page=pos'</script>";
	exit;
}

function checkoutTableReserved(){
	
	$id = $_GET['id'];

	$sql = "UPDATE tbl_reservation SET status = 'D' WHERE reservation_id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	echo "<script>alert('Reserved Successfully!!');</script>";
	echo "<script>document.location.href = '../index.php?page=table'</script>";
	exit;
}

function cancelTableReserved(){
	
	$id = $_GET['id'];

	$sql = "UPDATE tbl_reservation SET status = 'C' WHERE reservation_id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	echo "<script>alert('Reservation Cancelled!!');</script>";
	echo "<script>document.location.href = '../index.php?page=table'</script>";
	exit;
}

function cancelOrderReserved(){
	
	$id = $_GET['id'];

	$sql = "UPDATE tbl_order_header SET status = 'C', transac_status = '--------' WHERE order_id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	echo "<script>alert('Order Cancelled!!');</script>";
	echo "<script>document.location.href = '../index.php?page=orders'</script>";
	exit;
}

function confirmOrderReserved(){

	$id = $_GET['id'];

	$sql = "UPDATE tbl_order_header SET status = 'S', transac_status = 'reserved' WHERE order_id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	echo "<script>alert('Order Reserved Successfully!!');</script>";
	echo "<script>document.location.href = '../index.php?page=orders'</script>";
	exit;
}

function finishedOrderReserved(){
	
	$id = $_GET['id'];

	$sql = "UPDATE tbl_order_header SET status = 'D', transac_status = '--------' WHERE order_id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	echo "<script>alert('Order Updated Successfully!!');</script>";
	echo "<script>document.location.href = '../index.php?page=orders'</script>";
	exit;
}
//-----------------------------------------------------------
//--------------Customer-------------------------------------
function newCustomer(){
	$name = $_POST['name'];
	$bday = $_POST['bday'];
	
	$query = "SELECT * FROM tbl_customer WHERE name = '$name'";
	$result = mysql_query($query) or die(mysql_error());
	
	$count = mysql_num_rows($result);
	if($count == 1){
		$_SESSION['memExist']['type'] = 'success';
		$_SESSION['memExist']['message'] = 'Record Already Exist';
		
		header("Location: ../index.php?page=customer");
	}else{
	$query = "INSERT INTO tbl_customer SET
			name = '$name', date_added = NOW(), status = 'active', birthday = '$bday'";
	$result = mysql_query($query) or die(mysql_error());
	
	$_SESSION['create']['type'] = 'success';
	$_SESSION['create']['message'] = 'New Record Added..';
	
	header("Location: ../index.php?page=customer");
	}
	exit;
}
function editCustomer(){
	
	$id = $_POST['id'];
	$firstname = $_POST['firstname'];
	$middlename = $_POST['middlename'];
	$lastname = $_POST['lastname'];

	$sql = "UPDATE tbl_pastor SET firstname = '$firstname',middlename = '$middlename',lastname = '$lastname' WHERE id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Edited Successfully..';
	
	header("Location: ../index.php?page=pastor");
	exit;
}
//---------------------------------------------------------
//--------------User-------------------------------------
function newUser(){
	$username = $_POST['username'];
	$desc = $_POST['description'];
	
	$query = "SELECT * FROM tbl_user WHERE username = '$username'";
	$result = mysql_query($query) or die(mysql_error());
	
	$count = mysql_num_rows($result);
	if($count == 1){
		$_SESSION['memExist']['type'] = 'success';
		$_SESSION['memExist']['message'] = 'Record Already Exist';
		
		header("Location: ../index.php?page=cpass");
	}else{
	$query = "INSERT INTO tbl_user SET
			username = '$username', password = '12345', description = '$desc', level = ''";
	$result = mysql_query($query) or die(mysql_error());
	
	$_SESSION['create']['type'] = 'success';
	$_SESSION['create']['message'] = 'New Record Added..';
	
	header("Location: ../index.php?page=cpass");
	}
	exit;
}
function editUser(){
	
	$id = $_POST['id'];
	$firstname = $_POST['firstname'];
	$middlename = $_POST['middlename'];
	$lastname = $_POST['lastname'];

	$sql = "UPDATE tbl_pastor SET firstname = '$firstname',middlename = '$middlename',lastname = '$lastname' WHERE id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Edited Successfully..';
	
	header("Location: ../index.php?page=pastor");
	exit;
}


function addProduct(){
	$stock = $_POST['stock'];
	$cat_id = $_POST['cat_id'];
	$type = $_POST['type'];
	$package = $_POST['package'];
	$supplier = $_POST['supplier'];
	$price1 = $_POST['price1'];
	$price2 = $_POST['price2'];
	$price3 = $_POST['price3'];
	$price4 = $_POST['price4'];
	$price5 = $_POST['price5'];
	
	$query = "SELECT * FROM tbl_product WHERE stock = '$stock'";
	$result = mysql_query($query) or die(mysql_error());
	
	$count = mysql_num_rows($result);
	if($count == 1){
		$_SESSION['memExist']['type'] = 'success';
		$_SESSION['memExist']['message'] = 'Record Already Exist';
		
		header("Location: ../index.php?page=products");
	}else{
	if(isset($_FILES['files'])){
    $errors= array();
	foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
		$file_name = $key.$_FILES['files']['name'][$key];
		$file_tmp = $_FILES['files']['tmp_name'][$key];
		$file_name = preg_replace("#[^a-z0-9.]#i", "", $file_name);
		
        if($file_size > 2097152){
			$errors[]='File size must be less than 2 MB';
        }		
        $query="INSERT INTO tbl_product SET stock = '$stock', category = '$cat_id', supplier_id = '$supplier', type = '$type',
				package_id = '$package', image = '$file_name', price1 = '$price1', price2 = '$price2', price3 = '$price3',
				price4 = '$price4', price5 = '$price5'";
			
        $desired_dir="user_data";
        if(empty($errors)==true){
            if(is_dir($desired_dir)==false){
                mkdir("$desired_dir", 0700);		// Create directory if it does not exist
            }
            if(is_dir("$desired_dir/".$file_name)==false){
                move_uploaded_file($file_tmp,"$desired_dir/".$file_name);
            }else{									// rename the file if another one exist
                $new_dir="$desired_dir/".$file_name.time();
                 rename($file_tmp,$new_dir) ;				
            }
		 mysql_query($query);			
        }else{
                print_r($errors);
        }
    }
	if(empty($error)){
		echo "Success";
	}
	$extensions = array("jpeg","jpg","png"); 
//$file_ext=explode('.',$_FILES['image']['name'][$key])	;
//$file_ext=end($file_ext);  
//$file_ext=strtolower(end(explode('.',$_FILES['image']['name'][$key])));  
//if(in_array($file_ext,$extensions ) === false){
	$errors[]="extension not allowed";  
		}
		
	$id = mysql_insert_id();
	
	$_SESSION['create']['type'] = 'success';
	$_SESSION['create']['message'] = 'Product Added';
		
	header("Location: ../index.php?page=products");
	}
	exit;
}
//---------------------------------------------------------
//--------------Employee-------------------------------------
function newEmployee(){
	$name = $_POST['name'];
	
	$query = "SELECT * FROM tbl_employee WHERE name = '$name'";
	$result = mysql_query($query) or die(mysql_error());
	
	$count = mysql_num_rows($result);
	if($count == 1){
		$_SESSION['memExist']['type'] = 'success';
		$_SESSION['memExist']['message'] = 'Record Already Exist';
		
		header("Location: ../index.php?page=employee");
	}else{
	$query = "INSERT INTO tbl_employee SET
			name = '$name', status = 'active', date_added = NOW()";
	$result = mysql_query($query) or die(mysql_error());
	
	$_SESSION['create']['type'] = 'success';
	$_SESSION['create']['message'] = 'New Record Added..';
	
	header("Location: ../index.php?page=employee");
	}
	exit;
}
function editEmployee(){
	
	$id = $_POST['id'];
	$firstname = $_POST['firstname'];
	$middlename = $_POST['middlename'];
	$lastname = $_POST['lastname'];

	$sql = "UPDATE tbl_pastor SET firstname = '$firstname',middlename = '$middlename',lastname = '$lastname' WHERE id='$id'";

	mysql_query($sql) or die(mysql_error());
	
	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Edited Successfully..';
	
	header("Location: ../index.php?page=pastor");
	exit;
}

function addPO(){
	
	$id = $_POST['supplier_id'];
	$date = $_POST['date'];
	$temp_po_id = $_POST['temp_po_id'];

	$stock_id = $_POST['stock_id'];

	$sql = "insert into tbl_po_header SET supplier_id = '$id',date_transac = '$date'";

	mysql_query($sql) or die(mysql_error());
	
	$po_header_id = mysql_insert_id();
	if(!empty($stock_id)){
		foreach($stock_id as $ch) {			
			$sql = "insert into tbl_po_details
					 SET 
						po_header_id = '$po_header_id',
						stock_id = '$ch',
						qty='".$_POST['qty_'.$ch]."',
						amount='".$_POST['amount_'.$ch]."',
						cost='".$_POST['cost_'.$ch]."'
						";

			mysql_query($sql) or die(mysql_error());
		}
	}
	
	mysql_query("DELETE FROM tbl_temp_po WHERE temp_po_id = '$temp_po_id'");
	mysql_query("DELETE FROM tbl_temp_po_details WHERE temp_po_id = '$temp_po_id'");

	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Save Successfully..';
	
	header("Location: ../index.php?page=po");
	exit;
}

function addFormulation(){
	
	$category_id = $_POST['category_id'];
	$stock_id = $_POST['stock_id'];
	$date = $_POST['date'];
	$code = $_POST['code'];


	$raw_id = $_POST['raw_id'];
	$qty = $_POST['qty'];
	$amount = $_POST['amount'];
	$temp_po_id = $_POST['temp_po_id'];

	$sql = "insert into tbl_formulation_header 
				SET 
					formulationcode = '$code',
					formulationdate = '$date',
					category = '$category_id',
					finishedproduct = '$stock_id'
				";

		mysql_query($sql) or die(mysql_error());
		
	$formulation_id = mysql_insert_id();
	if(!empty($stock_id)){
		$x=0;
		foreach($raw_id as $ch) {			
			$sql = "insert into tbl_formulation_details
					 SET 
						formulation_id = '$formulation_id',
						stock_id = '$ch',
						quantity='".$qty[$x]."',
						amount='".$amount[$x]."'
						";

			mysql_query($sql) or die(mysql_error());

			$x++;
		}
	}
	
	mysql_query("DELETE FROM tbl_temp_formula WHERE temp_formula_id = '$temp_po_id'");
	mysql_query("DELETE FROM tbl_temp_formula_header WHERE temp_formulation_id = '$temp_po_id'");

	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Save Successfully..';
	
	header("Location: ../index.php?page=formulation");
	exit;
}

function addTempStock(){
	
	$id = $_POST['supplier_id'];
	$date = $_POST['date'];
	$stock_id = $_POST['stock_id'];
	$qty = $_POST['qty'];
	$temp_po_id = $_POST['temp_po_id'];

	if(empty($temp_po_id)){
		$sql = "insert into tbl_temp_po SET supplier_id = '$id',date_transac = '$date'";

		mysql_query($sql) or die(mysql_error());
		
		$temp_po_id = mysql_insert_id();
	}

	$sql = "insert into tbl_temp_po_details SET temp_po_id = '$temp_po_id',stock_id = '$stock_id',qty='$qty'";

	mysql_query($sql) or die(mysql_error());

	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Added Successfully..';

	echo $temp_po_id;
}

function addTempF(){
	
	$category_id = $_POST['category_id'];
	$stock_id = $_POST['stock_id'];
	$date = $_POST['date'];
	$code = $_POST['code'];


	$raw_id = $_POST['raw_id'];
	$qty = $_POST['qty'];
	$temp_po_id = $_POST['temp_po_id'];


	

	//GET INVENTORY BALANCE
	//$balance = getCurrentBalanceRaw($raw_id,$date);
	#return $balance;
	/*if($balance<=$qty){
		echo "Not Enough Inventory Balance.";
	}else{
	*/
		if(empty($temp_po_id)){
			$sql = "insert into tbl_temp_formula_header 
					SET 
						formulationcode = '$code',
						formulationdate = '$date',
						category = '$category_id',
						finishedproduct = '$stock_id'
					";

			mysql_query($sql) or die(mysql_error());
			
			$temp_po_id = mysql_insert_id();
		}
		$amount = getAttribute("tbl_product","stock_id",$raw_id,"cost") * $qty;

		$sql = "insert into tbl_temp_formula
				 SET 
				 	temp_formula_id = '$temp_po_id',
				 	stock_id = '$raw_id',
				 	quantity='$qty',
				 	amount = '$amount'
				 	";

		mysql_query($sql) or die(mysql_error());

		$_SESSION['edit']['type'] = 'success';
		$_SESSION['edit']['message'] = 'Added Successfully..';

		echo $temp_po_id;
	//}
	
}

function getTempStocks(){
	$temp_po_id = $_POST['temp_po_id'];
	$html='<table border="0" width="100%" id="product_list">
			<tr>
				<th>Product List</th>
			</tr>
			<tr bgcolor="#C0C0C0">
				<td>Stock</td>
				<td>Quantity</td>
				<td>Amount</td>
		  </tr>';

	$sql=mysql_query("SELECT * FROM tbl_temp_po_details WHERE temp_po_id = '$temp_po_id'");
	while($r=mysql_fetch_assoc($sql)){
		$cost = getAttribute("tbl_product","stock_id",$r['stock_id'],"cost");
		$amount = $cost * $r['qty'];
		$html.="<tr>
					<td>".getAttribute("tbl_product","stock_id",$r['stock_id'],"stock")."<input type='hidden' name='stock_id[]' value='".$r['stock_id']."'></td>
					<td>".$r['qty']."<input type='hidden' name='qty_$r[stock_id]' value='".$r['qty']."'><input type='hidden' name='cost_$r[stock_id]' value='".$cost."'></td>
					<td style='text-align:right;'>".number_format($amount,2)." <input type='hidden' name='amount_$r[stock_id]' value='".$amount."'></td>
				</tr>";
	}

	$html.="</table>";

	echo $html;

}

function getTempF(){
	$temp_po_id = $_POST['temp_po_id'];
	$html='<table border="0" width="100%" id="product_list">
			<tr>
				<th>Ingredients</th>
			</tr>
			<tr bgcolor="#C0C0C0">
				<td>Stock</td>
				<td>Quantity</td>
				<td>Amount</td>
		  </tr>';

	$sql=mysql_query("SELECT * FROM tbl_temp_formula WHERE temp_formula_id = '$temp_po_id'");
	while($r=mysql_fetch_assoc($sql)){
		$cost = getAttribute("tbl_product","stock_id",$r['stock_id'],"cost");
		
		$html.="<tr>
					<td>".getAttribute("tbl_product","stock_id",$r['stock_id'],"stock")."<input type='hidden' name='raw_id[]' value='".$r['stock_id']."'></td>
					<td>".$r['quantity']."<input type='hidden' name='qty[]' value='".$r['quantity']."'><input type='hidden' name='cost[]' value='".$cost."'></td>
					<td style='text-align:right;'>".number_format($r['amount'],2)." <input type='hidden' name='amount[]' value='".$r['amount']."'></td>
				</tr>";
	}

	$html.="</table>";

	echo $html;

}

function cancelPO(){
	
	$po_header_id = $_GET['po_header_id'];

	$sql = "UPDATE tbl_po_header SET status = 'C' WHERE po_header_id='$po_header_id'";

	mysql_query($sql) or die(mysql_error());
	
	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Cancelled Successfully..';
	
	header("Location: ../index.php?page=po");
	exit;
}

function cancelRR(){
	
	$rr_header_id = $_GET['rr_header_id'];

	$sql = "UPDATE tbl_rr_header SET status = 'C' WHERE rr_header_id='$rr_header_id'";

	mysql_query($sql) or die(mysql_error());
	
	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Cancelled Successfully..';
	
	header("Location: ../index.php?page=po");
	exit;
}

function deleteProduct(){
	
	$stock_id = $_GET['stock_id'];

	$sql = "DELETE FROM tbl_product WHERE stock_id = '$stock_id'";

	mysql_query($sql) or die(mysql_error());
	
	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Cancelled Successfully..';
	
	header("Location: ../index.php?page=products");
	exit;
}

function getPOTableForRR(){
			$po_header_id = $_POST['po_header_id'];
			
			$content='	
				<table cellspacing="2" cellpadding="5" width="100%" align="center">
					<tr bgcolor="#C0C0C0">				
						<th><b>Stock</b></th>
						<th><b>PO Quantity</b></th>
						<th><b>Cost</b></th>
						<th><b>Received Quantity</b></th>
					</tr> 
				';
				
				$query="
					select 
						*
					from
						tbl_po_details
					where
						po_header_id='$po_header_id'
				";
				
				$result=mysql_query($query);
				
				$totalamount=0;
				$i=1;
				while($r=mysql_fetch_assoc($result)):
					$totalamount+=$r['amount'];		
					$qty_received=getQuantityOfReceivedStock($po_header_id,$r['stock_id']);
					
					$u_quantity=$r['qty']-$qty_received;
					if($u_quantity!=0){
						$content.=' 
							<tr>				
								<td>'.getAttribute("tbl_product","stock_id",$r['stock_id'],"stock").'</td>
								<td><div align="center">'.number_format($u_quantity,0,'.',',').'</div></td>
								<td><div align="right">P '.number_format($r['cost'],2,'.',',').' </div></td>
								<td><input type="text" class="textbox3" name="delivered_amount[]"  ></td>
								<input type="hidden" name="cost[]" value="'.$r['cost'].'" />
								<input type="hidden" name="stock_id[]" value="'.$r['stock_id'].'" >
							</tr>   
						';
					}
					
				endwhile;
		
	echo $content;
}

function addRR(){
		$stock_id			= $_REQUEST[stock_id];
		$cost				= $_REQUEST[cost];
		$delivered_amount	= $_REQUEST[delivered_amount];	
		
		$po_header_id		= $_REQUEST[po_header_id];
		$date				= $_REQUEST[date];
		$supplier_id		= getAttribute("tbl_po_header","po_header_id",$po_header_id,"supplier_id");

		$sql = "insert into tbl_rr_header SET po_header_id = '$po_header_id',date_transac = '$date',supplier_id='$supplier_id'";

		mysql_query($sql) or die(mysql_error());
		
		$rr_header_id = mysql_insert_id();

		$x=0;
		
		foreach($stock_id as $stock):
			$amount=$delivered_amount[$x]*$cost[$x];
			
			mysql_query("
				insert into
					tbl_rr_details
				set
					rr_header_id='$rr_header_id',
					stock_id='$stock',
					qty='$delivered_amount[$x]',
					cost='$cost[$x]',
					amount='$amount'
			") or die(mysql_error());
			$x++;
		endforeach;


	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Cancelled Successfully..';
	
	header("Location: ../index.php?page=sr");
	exit;
}

function checkCureentStocks(){
	$formulation_id = $_REQUEST['formulation_id'];
	$date = $_REQUEST['date'];
	$qty = $_REQUEST['qty'];

	$sql=mysql_query("SELECT * FROM tbl_formulation_details WHERE formulation_id = '$formulation_id'");

	$negativestocks="Not enough stocks for the ff:<br/>";
	while($r=mysql_fetch_assoc($sql)){
		$joborderqty = $r['quantity'] * $qty;
		
		$balance = getCurrentBalanceRaw($r['stock_id'],$date);
		if($balance<$joborderqty){
			$negativestocks.= "&nbsp;<b>".getAttribute("tbl_product","stock_id",$r['stock_id'],"stock").' - '.$balance.' Qty</b><br/>';
		}
	}
	if($negativestocks == "Not enough stocks for the ff:<br/>"){
		echo "<font color='green'>Balanced.</font>";
	}else{
		echo "<font color='red'>".$negativestocks."</font>";
	}
}

function addproduction(){
	$formulation_id = $_REQUEST['formulation_id'];
	$date = $_REQUEST['date'];
	$qty = $_REQUEST['qty'];
	$package_id = $_REQUEST['package_id'];
	$finishedproduct = getAttribute("tbl_formulation_header","formulation_id",$formulation_id,"finishedproduct");

	$sql=mysql_query("INSERT INTO 
						tbl_joborder_header
					  SET
					  	datefinished = '$date',
					  	qty = '$qty',
					  	typeofpackage = '$package_id',
					  	finishedproduct = '$finishedproduct',
					  	formulation_id = '$formulation_id'
					 ");
	$joborder_id = mysql_insert_id();

	$sql2=mysql_query("SELECT * FROM tbl_formulation_details WHERE formulation_id = '$formulation_id'");

	$price = 0;
	while($r=mysql_fetch_assoc($sql2)){
			$cost = $r['amount']/$r['quantity'];
			$price +=$r['amount'];
			mysql_query("INSERT INTO
							tbl_joborder_details
						 SET
						 	joborder_id = '$joborder_id',
						 	material = '$r[stock_id]',
						 	quantity = '$r[quantity]',
						 	cost = '$cost'
					   ");
	}

	switch ($package_id) {
		case '1':
			mysql_query("update tbl_product set price1 = '$price' WHERE stock_id = '$finishedproduct'");
		break;
		case '2':
			mysql_query("update tbl_product set price2 = '$price' WHERE stock_id = '$finishedproduct'");
		break;

		case '3':
			mysql_query("update tbl_product set price3 = '$price' WHERE stock_id = '$finishedproduct'");
		break;

		case '4':
			mysql_query("update tbl_product set price4 = '$price' WHERE stock_id = '$finishedproduct'");
		break;

		case '5':
			mysql_query("update tbl_product set price5 = '$price' WHERE stock_id = '$finishedproduct'");
		break;

		default:
			# code...
			mysql_query("update tbl_product set price1 = '$price' WHERE stock_id = '$finishedproduct'");
		break;
	}

	$_SESSION['edit']['type'] = 'success';
	$_SESSION['edit']['message'] = 'Added Successfully..';
	
	header("Location: ../index.php?page=prod");
	exit;
}
//---------------------------------------------------------
?>