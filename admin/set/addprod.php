<?php
	include '../library/config.php';
?>
<div class="pop-style">
<h2>Add Production</h2>
	<form action="set/process.php?action=addproduction" method="POST">
		Formulation: <br/>
		<?php
			echo getFormulationOpt(NULL,"formulation_id");
		?>
		<br/><br/>
		Date:<br/>
		<input type="date" name="date" id="date" required="required" class="form" placeholder="Transaction Date" />
		<br/><br/>
		Type of Package: <br/>
		<?php
			echo getPackage(NULL,"package_id");
		?>
		<br/><br/>
		Quantity:<br/>
			<input type="number" name="qty" id="qty" required="required" class="form" placeholder="qty" /><br/><br/> <span id="status"></span>
			<br/><br/>
		
		<input type="submit" value="Save"/>
	</form>
</div>
<script type="text/javascript">
	jQuery('#qty').keyup(function(){
		var formulation_id = jQuery('#formulation_id').val();
		var date = jQuery('#date').val();
		if(this.value!=""){
			if((formulation_id=="") || (date=="")){
				jQuery('#status').html("<font color='red'>Please Specify date or Formulation.</font>");
			}else{
				jQuery('#status').html("");
				jQuery.post("set/process.php?action=checkCureentStocks",{formulation_id: formulation_id,date:date,qty:this.value},function(a){
					jQuery('#status').html(a);
				});
			}
		}else{
			jQuery('#status').html("");
		}
	});	
</script>