<?php
	include '../library/config.php';

	echo 'Product: <br/>'.getProductsPerSupplier(NULL,"stock_id",$_REQUEST['id']).'<br/><br/>';
	echo 'Quantity: <br/><input type="number" name="quantity" id="quantity" class="form" required placeholder="qty">&nbsp;&nbsp;
			<input type="button" id="button_add" value="Add"><br/><br/>';
	echo '<input type="hidden" name="temp_po_id" id="temp_po_id">';

	echo '<div id="list">
			<table border="0" width="100%" id="product_list">
			<tr>
				<th>Product List</th>
			</tr>';
	echo '<tr bgcolor="#C0C0C0">
				<td>Stock</td>
				<td>Quantity</td>
				<td>Amount</td>
		  </tr>';
	echo '</table>
		  </div>';
?>

<script>
	jQuery('#button_add').click(function(){
		var sup_id = jQuery('#supplier_id').val();
		var date = jQuery('#date').val();

		if((date == "") || (sup_id == "")){
			alert("Fill in Date or Supplier First.");
		}else{
			var stock_id = jQuery('#stock_id').val();
			var qty = jQuery('#quantity').val();
			var temp_po_id = jQuery('#temp_po_id').val();

			if((stock_id == "") || (qty == "")){
				alert("Select Product or supply a quantity.");
			}else{
				jQuery.post("set/process.php?action=addTempstock",{temp_po_id:temp_po_id,supplier_id:sup_id,date:date,stock_id:stock_id,qty:qty},
				function(e){
					jQuery('#temp_po_id').val(e);
					jQuery.post("set/process.php?action=getTempStocks",{temp_po_id:e},
					function(a){
						jQuery('#list').html(a);
					});
				});
			}
		}
	});
</script>