<?php include '../library/config.php'; ?>

   <link rel="stylesheet" href="../css/admin.css">
   <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
   <script type="text/javascript" src="../js/script.js"></script>
   
<div class="pop-style">
<h2>Add Product</h2><br/>
	<form class="prodform" action="set/process.php?action=addproduct" method="POST" enctype="multipart/form-data">
		<div id="form">
				<input type="text" name="stock" placeholder="Product Name" required/>
				<span>Category</span>
				<select name="cat_id" onchange="catFunctions();" id="category">
					<?php
						$get_cat = getCatList();
						foreach($get_cat as $catValue){
					?>
						<option value="<?php echo $catValue['cat_id'];?>"><?php echo $catValue['category'];?></option>
				<script>
					function catFunctions(){
						var x = document.getElementById("category").value;

						/*if(x == "ingredients"){
							document.getElementById("input").innerHTML = "<span>Package</span><input type='hidden' name='type' value='FP'/><select name='package'><option value='6'>None</option>";
							document.getElementById("supplier").innerHTML = "<span>Package</span><input type='hidden' name='type' value='RP'/><select name='package'><option value='6'>None</option></select>&nbsp;<span id='sup-lbl'>Supplier</span><select name='supplier'><?php $supplier = getSupplierList(); foreach($supplier as $supplierData){?><option value='<?php echo $supplierData['supplier_id'];?>'><?php echo $supplierData['name'];}?></option></select>"
						}else{
							document.getElementById("input").innerHTML = "<span>Package</span><input type='hidden' name='type' value='FP'/><select name='package'><?php $package = getPackageList(); foreach($package as $packData){?><option value='<?php echo $packData['package_id'];?>'><?php echo $packData['desc'];}?></option></select>";
						}*/
						if(x=='6'){
							document.getElementById("input").innerHTML = "";
							document.getElementById("supplier").innerHTML = "";

							document.getElementById("input").innerHTML = "<input type='hidden' name='type' value='FP'/>";
							document.getElementById("input").innerHTML += "<h3>Packages Prices</h3>";
							document.getElementById("input").innerHTML += "<span>1. Regular</span><input type='text' name='price1' placeholder='00.00' onkeypress='return isNumberKey(event)'/>";
							document.getElementById("input").innerHTML += "<span>2. Family</span><input type='text' name='price2' placeholder='00.00' onkeypress='return isNumberKey(event)'/></br></br>";
							document.getElementById("input").innerHTML += "<span>3. Grande</span><input type='text' name='price3' placeholder='00.00' onkeypress='return isNumberKey(event)'/>";
							document.getElementById("input").innerHTML += "<span>4. Party</span><input type='text' name='price4' placeholder='00.00' onkeypress='return isNumberKey(event)'/></br></br>";
							document.getElementById("input").innerHTML += "<span>5. Supreme</span><input type='text' name='price5' placeholder='00.00' onkeypress='return isNumberKey(event)'/>";
						}else if(x=='7'){
							document.getElementById("input").innerHTML = "";
							document.getElementById("supplier").innerHTML = "<span>Package</span><input type='hidden' name='type' value='RP'/><select name='package'><option value='6'>None</option></select>&nbsp;<span id='sup-lbl'>Supplier</span><select name='supplier'><?php $supplier = getSupplierList(); foreach($supplier as $supplierData){?><option value='<?php echo $supplierData['supplier_id'];?>'><?php echo $supplierData['name'];}?></option></select></br></br>";
							document.getElementById("supplier").innerHTML += "<span>Price</span><input type='text' name='price1' placeholder='00.00' onkeypress='return isNumberKey(event)'/>";
						}else{
							document.getElementById("input").innerHTML = "";
							document.getElementById("supplier").innerHTML = "";
							document.getElementById("input").innerHTML = "<span>Package</span><input type='hidden' name='type' value='FP'/><select name='package'><option value='6'>None</option></select></br></br>";
							document.getElementById("input").innerHTML += "<span>Price</span><input type='text' name='price1' placeholder='00.00' onkeypress='return isNumberKey(event)'/>";
						}
					}
				</script>
				<?php
						}
					?>
				</select>
				<category id="input"></category>
				<supplier id="supplier"></supplier>
		</div><br/><br/>
		<span>Upload an Image</span><input type="file" name="files[]" value=""/>
		<input type="submit" value="Save" class="myButton"/>
	</form>
</div>
