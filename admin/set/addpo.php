<?php
	include '../library/config.php';
?>
<div class="pop-style">
<h2>Add Purchase Order</h2>
	<form action="set/process.php?action=addpo" method="POST">
		Supplier: <br/>
		<?php
			echo getSupplier(NULL,"supplier_id");
		?>
		<br/><br/>
		Date:<br/>
		<input type="date" name="date" id="date" required="required" class="form" placeholder="Transaction Date" />
		<br/><br/>
		
		<div id="po_details">

		</div>
		<br/><br/>
		<input type="submit" value="Save"/>
	</form>
</div>
<script type="text/javascript">
	jQuery('#supplier_id').change(function(){
		jQuery.post("set/po_detail_form.php",{id: this.value},function(a){
			jQuery('#po_details').html(a);
		});
	});	
</script>