<link rel="stylesheet" type="text/css" media="screen" href="css/modal.css" />
<div class="cnt">
	<nav>
		<ul>
			<li><a href="index.php?page=cpass">Users</a></li>
			<li><a href="#" data-modal-open="modal-1">Add User</a></li>
		</ul>
	</nav>
	<div class="body-content">
		<div id="text">Users List</div>
		<div id="pagination" cellspacing="0">
		</div>
		<script type="text/javascript" src="paging/scriptuser.js"></script>
	</div>
</div>
<div id="modal-1" class="mb-modal">
<div class="close-modal">&#215;</div>
<div class="pop-style">
<h2>Add a User</h2>
	<form action="set/process.php?action=adduser" method="POST">
		<input type="text" name="username" placeholder="username" required/>
		<select name="description" style="padding:10px;">
			<option value="admin">Admin</option>
			<option value="cashier">Cashier</option>
			<option value="waiter">Waiter</option>
		</select>
		<input type="submit" value="Save"/>
	</form>
</div>
</div>
<script src="js/modal.min.js"></script>