<?php
require_once '../library/config.php';
?>
  <script type="text/javascript" src="../js/script.js"></script>
<style>
form input[type="text"]{
	padding: 10px;
}
form span{
	color: green;
	position: absolute;
	font-style: italic;
	font-weight: bold;
	margin-top: -20px;
}
form select{
	padding: 10px;
}
.myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #caefab;
	-webkit-box-shadow:inset 0px 1px 0px 0px #caefab;
	box-shadow:inset 0px 1px 0px 0px #caefab;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77d42a), color-stop(1, #5cb811));
	background:-moz-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-webkit-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-o-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:-ms-linear-gradient(top, #77d42a 5%, #5cb811 100%);
	background:linear-gradient(to bottom, #77d42a 5%, #5cb811 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77d42a', endColorstr='#5cb811',GradientType=0);
	background-color:#77d42a;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #268a16;
	display:inline-block;
	cursor:pointer;
	color:#306108;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:10px 15px;
	text-decoration:none;
	text-shadow:0px 1px 0px #aade7c;
	float: right;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cb811), color-stop(1, #77d42a));
	background:-moz-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-webkit-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-o-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:-ms-linear-gradient(top, #5cb811 5%, #77d42a 100%);
	background:linear-gradient(to bottom, #5cb811 5%, #77d42a 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cb811', endColorstr='#77d42a',GradientType=0);
	background-color:#5cb811;
}
.myButton:active {
	position:relative;
	top:1px;
}
.cnt-edit{
	margin-left: auto;
	margin-right: auto;
	width: 505px;
}
h3{
	text-align: center;
}
</style>
<br/><br/>
<div class="cnt-edit">
<h3>Edit Product</h3><br/>
<form method="POST" action="set/editProcess.php?action=editProduct">
<?php
$id = $_GET['id'];
$getDetails = getProductDetails($id);
foreach($getDetails as $value){
?>
	<input type="hidden" name="id" value="<?php echo $value['stock_id'];?>"/>
	<span>Stock Name</span><input type="text" name="stock" value="<?php echo $value['stock'];?>">
	<span>Description</span><input type="text" name="description" value="<?php echo $value['description'];?>">
	<span>Category</span>
	<select name="category">
		<option value="<?php echo $value['category'];?>"><?php echo getAttribute("tbl_category","cat_id",$value['category'],"category");?></option>
	</select>
	<?php
	if($value['type']=='RP'){
	?><br/><br/><br/>
	<span>Supplier</span>
	<select name="supplier">
		<option value="<?php echo $value['supplier_id'];?>"><?php echo getAttribute("tbl_supplier","supplier_id",$value['supplier_id'],"name");?></option>
	<?php
		$getSup = getSupplierList();
		foreach($getSup as $list){
	?>
		<option value="<?php echo $list['supplier_id'];?>"><?php echo $list['name'];?></option>
	<?php
		}
	?>
	</select>
<?php
	}if($value['category']=='6'){
?>
<br/>
	<h4>Packages Prices</h4>
	<span>1. Regular</span><input type='text' name='price1' value='<?php echo $value['price1']?>' onkeypress='return isNumberKey(event)'/>
	<span>2. Family</span><input type='text' name='price2' value='<?php echo $value['price2']?>' onkeypress='return isNumberKey(event)'/></br></br>
	<span>3. Grande</span><input type='text' name='price3' value='<?php echo $value['price3']?>' onkeypress='return isNumberKey(event)'/>
	<span>4. Party</span><input type='text' name='price4' value='<?php echo $value['price4']?>' onkeypress='return isNumberKey(event)'/></br></br>
	<span>5. Supreme</span><input type='text' name='price5' value='<?php echo $value['price5']?>' onkeypress='return isNumberKey(event)'/>
<?php
	}else{
		?>
		<h4>Package Price</h4>
		<span>Price</span>
		<input type='text' name='price1' id='value1' value='<?php echo $value['price1']?>'/>
		<?php
	}
}
?>
<br/><br/>
<input type="submit" value="Update!" class="myButton">
</form>
</div>
<br/><br/><br/><br/>