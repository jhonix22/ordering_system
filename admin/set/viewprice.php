<style>
.tbl-dis{
	width: 100%;
	background: #f8f8f8;
}
.tbl-dis th{
	text-align: center;
	background: #435229;
	color: #fff;
	padding: 5px;
	border: 1px solid #637B38;
}
.tbl-dis tr td{
	padding: 10px;
	border: 1px solid #000;
	text-align: center;
}
.tbl-dis tr{
	background: rgb(255,255,255);
	background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(229,229,229,1) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(229,229,229,1)));
	background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 );

}
.tbl-dis tr:hover{
	background: #fff;
}
</style>
<br/>
<?php
include '../library/config.php';
$id = $_GET['id'];
$getprice = getProductDetails($id);
foreach($getprice as $display){
	?>
<h3><?php echo $display['stock'];?></h3>
	<table class="tbl-dis">
			<th>Regular</th><th>Family</th><th>Grande</th><th>Party</th><th>Supreme</th>
						<tr>
							<td><?php echo $display['price1'];?></td>
							<td><?php echo $display['price2'];?></td>
							<td><?php echo $display['price3'];?></td>
							<td><?php echo $display['price4'];?></td>
							<td><?php echo $display['price5'];?></td>
						</tr>
		</table>
	<?php
}
?>