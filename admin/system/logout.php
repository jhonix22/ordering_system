<?php
require_once '../library/config.php';

session_destroy();

header("Location: ../index.php");
exit;

?>