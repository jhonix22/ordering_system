<?php
include '../library/config.php';

if(isset($_SESSION['login'])&& $_SESSION['login'] != ' '){
	header("Location: ../index.php");
	exit;
}

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Klarisa Login</title>
<link href="../css/login.css" rel='stylesheet' type='text/css' />
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
<!--webfonts-->
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic|Oswald:400,300,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,700,800' rel='stylesheet' type='text/css'>
<!--//webfonts-->
</head>
	</head>
	<body>
	<div class="span"></div>
		<div class="login-10">
			<div class="tenth-login">
				<h4>Klarisa Pizzeria</h4>
				<form class="ten" method="POST" action="process.php">
					<li class="cream">
						<input type="text" class="text" name="username" placeholder="Username" required><a href="#" class=" icon10 user10"></a>
					</li>
					<li class="cream">
						<input type="password" name="password" placeholder="Password" required><a href="#" class=" icon10 lock10"></a>
					</li>
					<div class="submit-ten">
						<input type="submit" value="Log in" > 
					</div><br/>
					<div id="messageLoginCon">
							<?php
								if((isset($_SESSION['loginCon']['type']) && 
								$_SESSION['loginCon']['message'] != '')){
								?>
								<div class="<?php echo $_SESSION['loginCon']['type'];?>" id="messagebox">
									<?php echo $_SESSION['loginCon']['message'];?>
								</div>
								<?php
								}
							?>
						</div>
				</form>
			</div>
		</div>
<?php
	unset($_SESSION['loginCon']['type']);
	unset($_SESSION['loginCon']['message']);
?>
	</body>
</html>