
<script type="text/javascript" src="stats/js/jquery-1.7.1.min.js" ></script>
<script type="text/javascript" src="stats/js/highcharts.js" ></script>
<script type="text/javascript" src="stats/js/themes/dark-blue.js"></script>

<script type="text/javascript">
	var chart;
			$(document).ready(function() {
				var options = {
					chart: {
						renderTo: 'container',
						defaultSeriesType: 'line',
						marginRight: 15,
						marginBottom: 25
					},
					title: {
						text: 'Monthly Sales',
						x: -20 //center
					},
					subtitle: {
						text: '',
						x: -20
					},
					xAxis: {
						type: 'datetime',
						//tickInterval: 3600 * 1000, // one hour
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'center',
							x: -3,
							y: 20,
							formatter: function() {
								return Highcharts.dateFormat('%m/%d/%y', this.value);
							}
						}
					},
					yAxis: {
						title: {
							text: 'Sales Amount'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function() {
								// show the combined time and date
								var isoDateTime = "%m-%d-%Y";
				                return Highcharts.dateFormat('', this.x-(1000*3600)) + 'Date: ' +Highcharts.dateFormat(isoDateTime, this.x) + '<br/>Amount:<b>'+ this.y + '</b>';
						}
					},
					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 100,
						borderWidth: 0
					},
					series: [{
						name: 'Sales Quantity'
					}]
				}
				jQuery.get('stats/data2.php', 
					null
					, function(tsv) {
					var lines = [];
					traffic = [];
					try {
						tsv = tsv.split(/\n/g);
						jQuery.each(tsv, function(i, line) {
							line = line.split(/\t/);
							date = Date.parse(line[0] +' UTC');
							traffic.push([
								date,
								parseInt(line[1].replace(',', ''), 10)
							]);
						});
					} catch (e) {  }
					options.series[0].data = traffic;
					chart = new Highcharts.Chart(options);
				});
			});
			
			function showGraph(){
				var options = {
					chart: {
						renderTo: 'container',
						defaultSeriesType: 'line',
						marginRight: 15,
						marginBottom: 25
					},
					title: {
						text: 'Monthly Sales',
						x: -20 //center
					},
					subtitle: {
						text: '',
						x: -20
					},
					xAxis: {
						type: 'datetime',
						//tickInterval: 3600 * 1000, // one hour
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'center',
							x: -3,
							y: 20,
							formatter: function() {
								return Highcharts.dateFormat('%m/%d/%y', this.value);
							}
						}
					},
					yAxis: {
						title: {
							text: 'Sales Amount'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function() {
								// show the combined time and date
								var isoDateTime = "%m-%d-%y";
				                return Highcharts.dateFormat('', this.x-(1000*3600)) + 'Date: ' +Highcharts.dateFormat(isoDateTime, this.x) + '<br/>Amount:<b>'+ this.y + '</b>';
						}
					},
					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 100,
						borderWidth: 0
					},
					series: [{
						name: 'Sales Quantity'
					}]
				}
				jQuery.get('stats/data.php', 
					{
					fromDate: $('#from').val(),
					toDate: $('#to').val()
					}, function(tsv) {
					var lines = [];
					traffic = [];
					try {
						// split the data return into lines and parse them
						tsv = tsv.split(/\n/g);
						jQuery.each(tsv, function(i, line) {
							line = line.split(/\t/);
							date = Date.parse(line[0] +' UTC');
							traffic.push([
								date,
								parseInt(line[1].replace(',', ''), 10)
							]);
						});
					} catch (e) {  }
					options.series[0].data = traffic;
					chart = new Highcharts.Chart(options);
				});
			}
</script>
<style>		
	#button{
		padding:5px;
		background:#2069B4;
		border-radius:6px;
		font-weight:bold;
		cursor:pointer;
		color:#FFFFFF;
	}
	#button:hover{
		padding:5px;
		background:#5081B3;
		border-radius:6px;
		font-weight:bold;
		cursor:pointer;
		color:#FFFFFF;
	}
</style>
<div style="margin-top: -40px;">
	<p style="font-weight:bold;">
	From: 
	<input type="date" id="from"/>
	To: 
	<input type="date" id="to"/>
	<input type="button" id="button" onclick="showGraph();" value="Generate"></p>
<div id="container" style="width: 100%; height: 490px; margin: -10px auto;"></div>
<div style="color:#CCC; font-size: 12px; font-weight: bold; width: 100%; background:#000; height:50px; margin-top:-15px; margin-left:auto; border-radius:0px 0px 6px 6px; position:relative;"><p style="padding:15px;" align="center">Month Date</p></div>
</div>