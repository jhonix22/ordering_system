<link rel="stylesheet" type="text/css" media="screen" href="css/modal.css" />
<div class="cnt">
	<nav>
		<ul>
			<li><a href="index.php?page=po">Purchae Orders</a></li>
			<li><a href="#" data-modal-open="modal-1">Add Purchase Order</a></li>
		</ul>
	</nav>
	<div class="body-content">
		<div id="text">Purchase Order</div>
		<div id="pagination" cellspacing="0">
		</div>
		<script type="text/javascript" src="paging/scriptpo.js"></script>
	</div>
</div>
<div id="modal-1" class="mb-modal">
<div class="close-modal">&#215;</div>
<div class="pop-style">
<h2>Add Purchase Order</h2>
	<form action="set/process.php?action=addpo" method="POST">
		Supplier: <br/>
		<?php
			echo getSupplier(NULL,"supplier_id");
		?>
		<br/><br/>
		Date:<br/>
		<input type="date" name="date" id="date" required="required" class="form" placeholder="Transaction Date" />
		<br/><br/>
		
		<div id="po_details">

		</div>
		<br/><br/>
		<input type="submit" value="Save"/>
	</form>
</div>
<script type="text/javascript">
	jQuery('#supplier_id').change(function(){
		jQuery.post("set/po_detail_form.php",{id: this.value},function(a){
			jQuery('#po_details').html(a);
		});
	});	
</script>
</div>
<script src="js/modal.min.js"></script>