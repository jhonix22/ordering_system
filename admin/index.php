<?php
include 'library/config.php';
$page = (isset($_GET['page']) && $_GET['page'] != '') ? $_GET['page'] : '';
$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';
checkUser();
?>
<!DOCTYPE html>
<html>
<head>
   <meta charpage='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/admin.css">
   <link rel="stylesheet" href="paging/style.css">
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
   <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
   
   <title>Klarisa Pizzeria</title>
</head>
<body onload="myFunction();">
 <?php
		$id = $_SESSION['sess_user_id'];
		$sql = "SELECT * FROM tbl_user WHERE user_id = '$id'";
		$result = mysql_query($sql);
		$row = mysql_fetch_assoc($result);
		extract($row);
 ?>
<div id='cssmenu'>
<ul>
<div class="lbl-det">
	<div class="lbl-spn"></div>
	<span class="login-details">Welcome <?php echo $row['username'];?></span>
</div>
   <li><span style="padding-right:320px; padding-left:10px;"><img src="../images/klarisa_pizzeria_logo.png" style="margin-top:6px;"/></span></li>
   <?php if($row['level'] == 1): ?>
   <li class="has-sub <?php if($page=='stat'||$page=='reports'){echo 'active';}?>"><a href="#"><span>Home</span></a>
		<ul>
		 <li class='has-sub'><a href='index.php?page=stat'><span>Statistics</span></a></li>
		 <li class='has-sub'><a href='index.php?page=reports'><span>Reports</span></a></li>
		</ul>
   </li>
   <li class="menu <?php if($page=='upfiles'){echo 'active';}?>"><a href='index.php?page=upfiles'><span>App</span></a></li>
   <script>
		$(document).ready(function(){
		setInterval(function(){
			$("#shownot").load('res/shownotif.php')
		}, 1000);
		});
	</script>
	<li class="has-sub <?php if($page=='table'||$page=='orders'){echo 'active';}?>" id="shownot"></li>
	<?php endif;?>
	<li class='has-sub <?php if($page=='po'||$page=='sr'||$page=='pos'||$page=='prod'||$page=='formulation'){echo 'active';}?>'><a href='#'><span>Transactions</span></a>
      <ul>
      	<?php if($row['level'] == 1): ?>
         <li class='has-sub'><a href='index.php?page=po'><span>Purchase Order</span></a></li>
         <li class='has-sub'><a href='index.php?page=sr'><span>Stock Receiving</span></a></li>
         <li class='has-sub'><a href='index.php?page=prod'><span>Production</span></a></li>
         <li class='has-sub'><a href='index.php?page=formulation'><span>Formulation</span></a></li>
        <?php endif; ?>
         <li class='has-sub'><a href='index.php?page=pos'><span>POS</span></a></li>
      </ul>
   </li>
   <?php if($row['level'] == 1): ?>
   <li class='has-sub <?php if($page=='supplier'||$page=='customer'||$page=='cpass'||$page=='employee'||$page=='products'){echo 'active';}?>'><a href='#'><span>Settings</span></a>
      <ul>

         <li class='has-sub'><a href='index.php?page=supplier'><span>Supplier</span></a></li>
         <li class='has-sub'><a href='index.php?page=customer'><span>Customer</span></a></li>
         <li class='has-sub'><a href='index.php?page=cpass'><span>Users</span></a></li>
         <!--<li class='has-sub'><a href='index.php?page=employee'><span>Employee</span></a></li>-->
         <li class='has-sub'><a href='index.php?page=products'><span>Products</span></a></li>
      </ul>
   </li>
	<?php endif; ?>
   <li class='last'><a href='system/logout.php' onclick="javascript: return confirm('Are you sure you want to logout?'); return false;"><span>Logout</span></a></li>
</ul>
</div><br/><br/><br/>
<script>
var myVar=setInterval(function(){myTimer()},1000);

function myTimer()
	{
var d=new Date();
var t=d.toLocaleTimeString();
document.getElementById("date_time").innerHTML=t;
}

function myFunction() {
	var d = new Date();
	var n = d.toDateString();
	document.getElementById("demo").innerHTML = n;
}
</script>
	<div class="bdy-cnt">
		<div class="time"><b id="demo"></b>&nbsp;<a id="date_time"></a><hr class="hori"></hr></div>
		<?php
		//---------------------------------reservations-tab------------------------------
			if($page == 'table'){
				require_once 'res/table.php';
				exit;
			}
			if($page == 'orders'){
				require_once 'res/orders.php';
				exit;
			}
		//---------------------------------settings-tab------------------------------
			if($page == 'supplier'){
				require_once 'set/supplier.php';
				exit;
			}
			if($page == 'customer'){
				require_once 'set/customer.php';
				exit;
			}
			if($page == 'employee'){
				require_once 'set/employee.php';
				exit;
			}
			if($page == 'products'){
				require_once 'set/products.php';
				exit;
			}
			if($page == 'cpass'){
				require_once 'set/changepass.php';
				exit;
			}
			if($page == 'supplier'){
				require_once 'set/supplier.php';
				exit;
			}
			
		//---------------------------------transaction-tab------------------------------
			if($page == 'po'){
				require_once 'transaction/po.php';
				exit;
			}
			if($page == 'sr'){
				require_once 'transaction/sr.php';
				exit;
			}
			if($page == 'pos'){
				require_once 'transaction/pos.php';
				exit;
			}
			if($page == 'prod'){
				require_once 'transaction/prod.php';
				exit;
			}

			if($page == 'formulation'){
				require_once 'transaction/formulation.php';
				exit;
			}
		//---------------------------------Upload-tab------------------------------
			if($page == 'upfiles'){
				require_once 'upload/index.php';
				exit;
			}
		//---------------------------------Stat-tab------------------------------
			if($page == 'stat'){
				require_once 'stats/monthlysales.php';
				exit;
			}
			if($page == 'reports'){
				require_once 'rep/reports.php';
				exit;
			}
		?>
	</div>
</body>
<html>
