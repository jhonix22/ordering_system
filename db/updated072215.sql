-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4975
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping structure for table db_capstone_order.tbl_category
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(10) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_category: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` (`cat_id`, `category`, `type`) VALUES
	(1, 'all', 'products'),
	(2, 'breakfast', 'products'),
	(3, 'special', 'products'),
	(4, 'desert', 'products'),
	(5, 'dinner', 'products'),
	(6, 'pizza', 'products');
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_po_details
CREATE TABLE IF NOT EXISTS `tbl_po_details` (
  `po_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`po_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_po_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_po_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_po_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_po_header
CREATE TABLE IF NOT EXISTS `tbl_po_header` (
  `po_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `date_transac` date NOT NULL DEFAULT '0000-00-00',
  `status` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`po_header_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_po_header: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_po_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_po_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_product
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `stock_id` int(10) NOT NULL AUTO_INCREMENT,
  `stock` varchar(100) NOT NULL,
  `stock_code` varchar(100) NOT NULL,
  `category` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `price1` decimal(12,2) NOT NULL,
  `price2` decimal(12,2) NOT NULL,
  `price3` decimal(12,2) NOT NULL,
  `price4` decimal(12,2) NOT NULL,
  `price5` decimal(12,2) NOT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_product: ~12 rows (approximately)
/*!40000 ALTER TABLE `tbl_product` DISABLE KEYS */;
INSERT INTO `tbl_product` (`stock_id`, `stock`, `stock_code`, `category`, `supplier_id`, `image`, `cost`, `price1`, `price2`, `price3`, `price4`, `price5`) VALUES
	(1, 'test1', '', 1, 0, 'food_icon01.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(2, 'test1', '', 2, 0, 'food_icon02.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(3, 'test1', '', 3, 0, 'food_icon03.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(4, 'test1', '', 4, 0, 'food_icon04.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(5, 'test1', '', 5, 0, 'food_icon05.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(6, 'test1', '', 6, 0, 'food_icon06.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(7, 'test1', '', 2, 0, 'food_icon07.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(8, 'test1', '', 3, 0, 'food_icon01.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(9, 'test1', '', 4, 0, 'food_icon02.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(10, 'test1', '', 5, 0, 'food_icon03.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(11, 'test1', '', 6, 0, 'food_icon04.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(12, 'test1', '', 7, 0, 'food_icon05.jpg', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
/*!40000 ALTER TABLE `tbl_product` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_rr_details
CREATE TABLE IF NOT EXISTS `tbl_rr_details` (
  `rr_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `rr_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`rr_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_rr_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_rr_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rr_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_rr_header
CREATE TABLE IF NOT EXISTS `tbl_rr_header` (
  `rr_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_header_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transac` date NOT NULL,
  `status` char(1) NOT NULL,
  PRIMARY KEY (`rr_header_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_rr_header: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_rr_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rr_header` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
