-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4975
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_capstone_order
CREATE DATABASE IF NOT EXISTS `db_capstone_order` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_capstone_order`;


-- Dumping structure for table db_capstone_order.tbl_contact
CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_contact: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_contact` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_order_details
CREATE TABLE IF NOT EXISTS `tbl_order_details` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`order_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_order_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_order_header
CREATE TABLE IF NOT EXISTS `tbl_order_header` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_transac` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  `discount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_order_header: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_order_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_pos_details
CREATE TABLE IF NOT EXISTS `tbl_pos_details` (
  `pos_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pos_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`pos_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_pos_details: 0 rows
/*!40000 ALTER TABLE `tbl_pos_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pos_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_pos_header
CREATE TABLE IF NOT EXISTS `tbl_pos_header` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_transac` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  `discount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_pos_header: 0 rows
/*!40000 ALTER TABLE `tbl_pos_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pos_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_reservation
CREATE TABLE IF NOT EXISTS `tbl_reservation` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `reservation_date` date NOT NULL,
  `phone` varchar(50) NOT NULL,
  `guest_number` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`reservation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_reservation: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reservation` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
