-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4975
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_capstone_order
CREATE DATABASE IF NOT EXISTS `db_capstone_order` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_capstone_order`;


-- Dumping structure for table db_capstone_order.tbl_category
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(10) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_category: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` (`cat_id`, `category`, `type`) VALUES
	(2, 'breakfast', 'products'),
	(3, 'special', 'products'),
	(4, 'desert', 'products'),
	(5, 'dinner', 'products'),
	(6, 'pizza', 'products'),
	(7, 'vegetables', 'ingredients');
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_contact
CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_contact: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_contact` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_customer
CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `customer_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL DEFAULT '12345',
  `status` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_customer: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_customer` DISABLE KEYS */;
INSERT INTO `tbl_customer` (`customer_id`, `name`, `contact`, `password`, `status`, `birthday`, `date_added`) VALUES
	(1, 'milky', '', '12345', 'active', '1989-09-03', '2015-07-25 15:25:18'),
	(17, 'roljhon', '0910239281', 'rolj', 'active', '2015-08-03', '2015-08-02 22:32:21');
/*!40000 ALTER TABLE `tbl_customer` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_employee
CREATE TABLE IF NOT EXISTS `tbl_employee` (
  `employee_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_employee: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_employee` DISABLE KEYS */;
INSERT INTO `tbl_employee` (`employee_id`, `name`, `status`, `date_added`) VALUES
	(1, 'Milky', 'active', '2015-07-25 16:02:44');
/*!40000 ALTER TABLE `tbl_employee` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_formulation_details
CREATE TABLE IF NOT EXISTS `tbl_formulation_details` (
  `formulationdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `formulation_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`formulationdetail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_formulation_details: 7 rows
/*!40000 ALTER TABLE `tbl_formulation_details` DISABLE KEYS */;
INSERT INTO `tbl_formulation_details` (`formulationdetail_id`, `formulation_id`, `stock_id`, `quantity`, `amount`) VALUES
	(1, 1, 10, 100, 1200.00),
	(2, 2, 12, 10, 120.00),
	(3, 2, 11, 15, 180.00),
	(4, 2, 10, 5, 60.00),
	(5, 3, 12, 10, 120.00),
	(6, 3, 11, 10, 120.00),
	(7, 3, 10, 10, 120.00);
/*!40000 ALTER TABLE `tbl_formulation_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_formulation_header
CREATE TABLE IF NOT EXISTS `tbl_formulation_header` (
  `formulation_id` int(11) NOT NULL AUTO_INCREMENT,
  `formulationcode` varchar(50) NOT NULL,
  `formulationdate` date NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `finishedproduct` int(11) NOT NULL,
  PRIMARY KEY (`formulation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_formulation_header: 3 rows
/*!40000 ALTER TABLE `tbl_formulation_header` DISABLE KEYS */;
INSERT INTO `tbl_formulation_header` (`formulation_id`, `formulationcode`, `formulationdate`, `category`, `description`, `finishedproduct`) VALUES
	(1, 't', '2015-07-31', 4, NULL, 9),
	(2, 'pizzaRegular', '2015-07-31', 6, NULL, 5),
	(3, 'HamCheease', '2015-07-31', 6, NULL, 13);
/*!40000 ALTER TABLE `tbl_formulation_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_joborder_details
CREATE TABLE IF NOT EXISTS `tbl_joborder_details` (
  `joborderdetail_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `joborder_id` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `cost` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`joborderdetail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_joborder_details: 7 rows
/*!40000 ALTER TABLE `tbl_joborder_details` DISABLE KEYS */;
INSERT INTO `tbl_joborder_details` (`joborderdetail_id`, `joborder_id`, `material`, `quantity`, `cost`) VALUES
	(1, 1, 10, 100, 12.00),
	(2, 2, 12, 10, 12.00),
	(3, 2, 11, 15, 12.00),
	(4, 2, 10, 5, 12.00),
	(5, 3, 12, 10, 12.00),
	(6, 3, 11, 10, 12.00),
	(7, 3, 10, 10, 12.00);
/*!40000 ALTER TABLE `tbl_joborder_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_joborder_header
CREATE TABLE IF NOT EXISTS `tbl_joborder_header` (
  `joborder_id` int(11) NOT NULL AUTO_INCREMENT,
  `datefinished` date NOT NULL,
  `qty` int(11) NOT NULL,
  `typeofpackage` int(11) NOT NULL,
  `finishedproduct` int(11) NOT NULL,
  `formulation_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`joborder_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_joborder_header: 3 rows
/*!40000 ALTER TABLE `tbl_joborder_header` DISABLE KEYS */;
INSERT INTO `tbl_joborder_header` (`joborder_id`, `datefinished`, `qty`, `typeofpackage`, `finishedproduct`, `formulation_id`, `status`) VALUES
	(1, '2015-07-31', 1, 6, 9, 1, 'S'),
	(2, '2015-08-05', 1, 1, 5, 2, 'S'),
	(3, '2015-08-05', 1, 1, 13, 3, 'S');
/*!40000 ALTER TABLE `tbl_joborder_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_order_details
CREATE TABLE IF NOT EXISTS `tbl_order_details` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`order_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_order_details: ~18 rows (approximately)
/*!40000 ALTER TABLE `tbl_order_details` DISABLE KEYS */;
INSERT INTO `tbl_order_details` (`order_detail_id`, `order_id`, `stock_id`, `package_id`, `qty`, `price`, `amount`) VALUES
	(1, 40, 2, 1, 1, 300.00, 300.00),
	(2, 40, 3, 5, 3, 900.00, 2700.00),
	(5, 40, 9, 6, 2, 300.00, 600.00),
	(6, 40, 9, 6, 80, 300.00, 24000.00),
	(7, 40, 2, 1, 5, 300.00, 1500.00),
	(8, 40, 13, 1, 2, 360.00, 720.00),
	(9, 42, 3, 3, 9, 300.00, 2700.00),
	(10, 42, 2, 2, 5, 300.00, 1500.00),
	(11, 43, 3, 3, 9, 300.00, 2700.00),
	(12, 43, 2, 2, 5, 300.00, 1500.00),
	(13, 44, 2, 2, 1, 300.00, 300.00),
	(14, 45, 2, 2, 1, 300.00, 300.00),
	(15, 46, 2, 2, 1, 300.00, 300.00),
	(16, 47, 9, 6, 1, 300.00, 300.00),
	(17, 47, 13, 2, 2, 360.00, 720.00),
	(18, 47, 7, 6, 1, 300.00, 300.00),
	(19, 47, 8, 6, 2, 300.00, 600.00),
	(20, 47, 6, 6, 1, 300.00, 300.00);
/*!40000 ALTER TABLE `tbl_order_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_order_header
CREATE TABLE IF NOT EXISTS `tbl_order_header` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_transac` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  `discount` decimal(12,2) NOT NULL,
  `loginstatus` int(11) NOT NULL DEFAULT '1',
  `transac_status` varchar(50) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_order_header: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_order_header` DISABLE KEYS */;
INSERT INTO `tbl_order_header` (`order_id`, `date_transac`, `customer_id`, `remarks`, `status`, `discount`, `loginstatus`, `transac_status`) VALUES
	(37, '2008-02-15', 1, '', 'S', 0.00, 1, 'pending'),
	(40, '2008-03-15', 17, '', 'S', 0.00, 1, 'pending'),
	(41, '2015-08-07', 0, '', 'S', 0.00, 1, 'pending'),
	(42, '2015-08-07', 0, 'test', 'S', 0.00, 1, 'pending'),
	(43, '2015-08-07', 0, 'test', 'S', 0.00, 1, 'pending'),
	(44, '2015-08-07', 17, '', 'S', 0.00, 1, 'pending'),
	(45, '2015-08-07', 17, '', 'S', 0.00, 1, 'pending'),
	(46, '2015-08-07', 17, 'tesy', 'S', 0.00, 1, 'pending'),
	(47, '2015-08-08', 17, 'test', 'S', 0.00, 1, 'pending'),
	(48, '2008-07-15', 17, '', 'S', 0.00, 1, 'pending');
/*!40000 ALTER TABLE `tbl_order_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_package
CREATE TABLE IF NOT EXISTS `tbl_package` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(100) NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_package: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_package` DISABLE KEYS */;
INSERT INTO `tbl_package` (`package_id`, `desc`) VALUES
	(1, 'Regular'),
	(2, 'Family'),
	(3, 'Grande'),
	(4, 'Party'),
	(5, 'Supreme'),
	(6, 'None');
/*!40000 ALTER TABLE `tbl_package` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_pos_details
CREATE TABLE IF NOT EXISTS `tbl_pos_details` (
  `pos_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pos_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`pos_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_pos_details: 0 rows
/*!40000 ALTER TABLE `tbl_pos_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pos_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_pos_header
CREATE TABLE IF NOT EXISTS `tbl_pos_header` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_transac` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  `discount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_pos_header: 0 rows
/*!40000 ALTER TABLE `tbl_pos_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pos_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_po_details
CREATE TABLE IF NOT EXISTS `tbl_po_details` (
  `po_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`po_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_po_details: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_po_details` DISABLE KEYS */;
INSERT INTO `tbl_po_details` (`po_detail_id`, `po_header_id`, `stock_id`, `cost`, `qty`, `amount`) VALUES
	(1, 1, 12, 12.00, 2, 24.00),
	(2, 2, 12, 12.00, 2, 24.00),
	(3, 2, 11, 12.00, 5, 60.00),
	(4, 2, 10, 12.00, 122, 1464.00),
	(5, 3, 12, 12.00, 500, 6000.00),
	(6, 3, 11, 12.00, 500, 6000.00),
	(7, 3, 10, 12.00, 500, 6000.00);
/*!40000 ALTER TABLE `tbl_po_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_po_header
CREATE TABLE IF NOT EXISTS `tbl_po_header` (
  `po_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `date_transac` date NOT NULL DEFAULT '0000-00-00',
  `status` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`po_header_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_po_header: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_po_header` DISABLE KEYS */;
INSERT INTO `tbl_po_header` (`po_header_id`, `supplier_id`, `date_transac`, `status`) VALUES
	(1, 12, '2015-07-01', 'C'),
	(2, 12, '2015-07-01', 'S'),
	(3, 12, '2015-08-04', 'S');
/*!40000 ALTER TABLE `tbl_po_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_product
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `stock_id` int(10) NOT NULL AUTO_INCREMENT,
  `stock` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `stock_code` varchar(100) NOT NULL,
  `category` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `package_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `price1` decimal(12,2) NOT NULL,
  `price2` decimal(12,2) NOT NULL,
  `price3` decimal(12,2) NOT NULL,
  `price4` decimal(12,2) NOT NULL,
  `price5` decimal(12,2) NOT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_product: ~13 rows (approximately)
/*!40000 ALTER TABLE `tbl_product` DISABLE KEYS */;
INSERT INTO `tbl_product` (`stock_id`, `stock`, `description`, `stock_code`, `category`, `supplier_id`, `type`, `package_id`, `image`, `cost`, `price1`, `price2`, `price3`, `price4`, `price5`) VALUES
	(2, 'test1', '', '', 2, 0, 'FP', 2, 'food_icon02.jpg', 0.00, 300.00, 400.00, 500.00, 600.00, 900.00),
	(3, 'test2', '', '', 2, 0, 'FP', 3, 'food_icon03.jpg', 0.00, 300.00, 400.00, 500.00, 600.00, 900.00),
	(4, 'test3', '', '', 6, 0, 'FP', 4, 'food_icon04.jpg', 0.00, 300.00, 400.00, 500.00, 600.00, 900.00),
	(5, 'test4', '', '', 6, 0, 'FP', 5, 'food_icon05.jpg', 0.00, 360.00, 400.00, 500.00, 600.00, 900.00),
	(6, 'test5', '', '', 5, 0, 'FP', 6, 'food_icon06.jpg', 0.00, 300.00, 400.00, 500.00, 600.00, 900.00),
	(7, 'test6', '', '', 3, 0, 'FP', 6, 'food_icon07.jpg', 0.00, 300.00, 400.00, 500.00, 600.00, 900.00),
	(8, 'test7', '', '', 3, 0, 'FP', 6, 'food_icon01.jpg', 0.00, 300.00, 400.00, 500.00, 600.00, 900.00),
	(9, 'desert', '', '', 4, 0, 'FP', 6, 'food_icon02.jpg', 0.00, 300.00, 400.00, 500.00, 600.00, 900.00),
	(10, 'vegetable1', '', '', 7, 12, 'RP', 6, 'food_icon03.jpg', 12.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(11, 'testvegetable2', '', '', 7, 12, 'RP', 6, 'food_icon04.jpg', 12.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(12, 'test1vegetable3', '', '', 7, 12, 'RP', 6, 'food_icon05.jpg', 12.00, 0.00, 0.00, 0.00, 0.00, 0.00),
	(13, 'Ham and Cheese', '', '', 6, 0, 'FP', 2, '0foodicon06.jpg', 0.00, 360.00, 0.00, 0.00, 0.00, 0.00),
	(14, 'Egg Plant', '', '', 7, 1, 'RP', 6, '0', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
/*!40000 ALTER TABLE `tbl_product` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_reservation
CREATE TABLE IF NOT EXISTS `tbl_reservation` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `reservation_date` date NOT NULL,
  `phone` varchar(50) NOT NULL,
  `guest_number` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `date_added` date NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`reservation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_reservation: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_reservation` DISABLE KEYS */;
INSERT INTO `tbl_reservation` (`reservation_id`, `firstname`, `lastname`, `state`, `reservation_date`, `phone`, `guest_number`, `email`, `subject`, `date_added`, `status`) VALUES
	(1, 'Roljhon', 'Teano', 'Bacolod City', '2015-08-07', '09215244621', 2, 'roljhon1991@gmail.com', 'Birthday', '2015-08-06', 'S'),
	(2, 'das', 'das', 'das', '2015-08-07', '09215244621', 5, 'roljhon1991@gmail.com', 'dsad', '2015-08-06', 'S');
/*!40000 ALTER TABLE `tbl_reservation` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_rr_details
CREATE TABLE IF NOT EXISTS `tbl_rr_details` (
  `rr_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `rr_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`rr_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_rr_details: ~11 rows (approximately)
/*!40000 ALTER TABLE `tbl_rr_details` DISABLE KEYS */;
INSERT INTO `tbl_rr_details` (`rr_detail_id`, `rr_header_id`, `stock_id`, `cost`, `qty`, `amount`) VALUES
	(1, 1, 12, 12.00, 2, 24.00),
	(2, 1, 11, 12.00, 3, 36.00),
	(3, 1, 10, 12.00, 100, 1200.00),
	(4, 2, 11, 12.00, 1, 12.00),
	(5, 2, 10, 12.00, 5, 60.00),
	(6, 3, 11, 12.00, 2, 24.00),
	(7, 3, 10, 12.00, 10, 120.00),
	(8, 4, 10, 12.00, 12, 144.00),
	(9, 5, 12, 12.00, 500, 6000.00),
	(10, 5, 11, 12.00, 500, 6000.00),
	(11, 5, 10, 12.00, 500, 6000.00);
/*!40000 ALTER TABLE `tbl_rr_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_rr_header
CREATE TABLE IF NOT EXISTS `tbl_rr_header` (
  `rr_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `po_header_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transac` date NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`rr_header_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_rr_header: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_rr_header` DISABLE KEYS */;
INSERT INTO `tbl_rr_header` (`rr_header_id`, `po_header_id`, `supplier_id`, `date_transac`, `status`) VALUES
	(1, 2, 12, '2015-07-30', 'S'),
	(2, 2, 12, '2015-07-30', 'C'),
	(3, 2, 12, '2015-04-30', 'S'),
	(4, 2, 12, '2015-07-31', 'S'),
	(5, 3, 12, '2015-08-04', 'S');
/*!40000 ALTER TABLE `tbl_rr_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_supplier
CREATE TABLE IF NOT EXISTS `tbl_supplier` (
  `supplier_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_supplier: ~12 rows (approximately)
/*!40000 ALTER TABLE `tbl_supplier` DISABLE KEYS */;
INSERT INTO `tbl_supplier` (`supplier_id`, `name`, `date_added`, `status`) VALUES
	(1, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(2, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(3, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(4, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(5, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(6, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(7, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(8, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(9, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(10, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(11, 'Seasons', '0000-00-00 00:00:00', 'active'),
	(12, 'Savemore', '2015-07-25 14:48:14', 'active');
/*!40000 ALTER TABLE `tbl_supplier` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_temp_formula
CREATE TABLE IF NOT EXISTS `tbl_temp_formula` (
  `temp_formula_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `temp_formula_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`temp_formula_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_temp_formula: 0 rows
/*!40000 ALTER TABLE `tbl_temp_formula` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_temp_formula` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_temp_formula_header
CREATE TABLE IF NOT EXISTS `tbl_temp_formula_header` (
  `temp_formulation_id` int(11) NOT NULL AUTO_INCREMENT,
  `formulationcode` varchar(50) NOT NULL,
  `formulationdate` date NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `finishedproduct` int(11) NOT NULL,
  PRIMARY KEY (`temp_formulation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_temp_formula_header: 0 rows
/*!40000 ALTER TABLE `tbl_temp_formula_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_temp_formula_header` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_temp_po
CREATE TABLE IF NOT EXISTS `tbl_temp_po` (
  `temp_po_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `date_transac` date NOT NULL,
  PRIMARY KEY (`temp_po_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_temp_po: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_temp_po` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_temp_po` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_temp_po_details
CREATE TABLE IF NOT EXISTS `tbl_temp_po_details` (
  `temp_po_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `temp_po_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`temp_po_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_temp_po_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_temp_po_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_temp_po_details` ENABLE KEYS */;


-- Dumping structure for table db_capstone_order.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `level` int(3) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table db_capstone_order.tbl_user: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_id`, `username`, `password`, `description`, `level`) VALUES
	(1, 'admin', 'admin', 'admin', 1),
	(3, 'cash123', '12345', 'cashier', 0),
	(4, 'test', '12345', 'cashier', 0),
	(5, 'waiter1', '12345', 'waiter', 0);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
