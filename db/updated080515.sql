-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2015 at 09:46 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_capstone_order`
--
--
--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
`cat_id` int(10) NOT NULL,
  `category` varchar(10) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cat_id`, `category`, `type`) VALUES
(2, 'breakfast', 'products'),
(3, 'special', 'products'),
(4, 'desert', 'products'),
(5, 'dinner', 'products'),
(6, 'pizza', 'products'),
(7, 'vegetables', 'ingredients');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
`contact_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE IF NOT EXISTS `tbl_customer` (
`customer_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL DEFAULT '12345',
  `status` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_id`, `name`, `contact`, `password`, `status`, `birthday`, `date_added`) VALUES
(1, 'milky', '', '12345', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(17, 'roljhon', '0910239281', 'rolj', 'active', '2015-08-03', '2015-08-02 22:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_employee` (
`employee_id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`employee_id`, `name`, `status`, `date_added`) VALUES
(1, 'Milky', 'active', '2015-07-25 16:02:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_formulation_details`
--

CREATE TABLE IF NOT EXISTS `tbl_formulation_details` (
`formulationdetail_id` int(11) NOT NULL,
  `formulation_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_formulation_details`
--

INSERT INTO `tbl_formulation_details` (`formulationdetail_id`, `formulation_id`, `stock_id`, `quantity`, `amount`) VALUES
(1, 1, 10, 100, '1200.00'),
(2, 2, 12, 10, '120.00'),
(3, 2, 11, 15, '180.00'),
(4, 2, 10, 5, '60.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_formulation_header`
--

CREATE TABLE IF NOT EXISTS `tbl_formulation_header` (
`formulation_id` int(11) NOT NULL,
  `formulationcode` varchar(50) NOT NULL,
  `formulationdate` date NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `finishedproduct` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_formulation_header`
--

INSERT INTO `tbl_formulation_header` (`formulation_id`, `formulationcode`, `formulationdate`, `category`, `description`, `finishedproduct`) VALUES
(1, 't', '2015-07-31', 4, NULL, 9),
(2, 'pizzaRegular', '2015-07-31', 6, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_joborder_details`
--

CREATE TABLE IF NOT EXISTS `tbl_joborder_details` (
`joborderdetail_id` bigint(12) NOT NULL,
  `joborder_id` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `cost` decimal(12,2) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_joborder_details`
--

INSERT INTO `tbl_joborder_details` (`joborderdetail_id`, `joborder_id`, `material`, `quantity`, `cost`) VALUES
(1, 1, 10, 100, '12.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_joborder_header`
--

CREATE TABLE IF NOT EXISTS `tbl_joborder_header` (
`joborder_id` int(11) NOT NULL,
  `datefinished` date NOT NULL,
  `qty` int(11) NOT NULL,
  `typeofpackage` int(11) NOT NULL,
  `finishedproduct` int(11) NOT NULL,
  `formulation_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_joborder_header`
--

INSERT INTO `tbl_joborder_header` (`joborder_id`, `datefinished`, `qty`, `typeofpackage`, `finishedproduct`, `formulation_id`, `status`) VALUES
(1, '2015-07-31', 1, 6, 9, 1, 'S');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE IF NOT EXISTS `tbl_order_details` (
`order_detail_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_detail_id`, `order_id`, `stock_id`, `package_id`, `qty`, `price`, `amount`) VALUES
(1, 40, 2, 1, 1, '300.00', '300.00'),
(2, 40, 3, 5, 3, '900.00', '2700.00'),
(5, 40, 9, 6, 2, '300.00', '600.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_header`
--

CREATE TABLE IF NOT EXISTS `tbl_order_header` (
`order_id` int(11) NOT NULL,
  `date_transac` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  `discount` decimal(12,2) NOT NULL,
  `loginstatus` int(11) NOT NULL DEFAULT '1',
  `transac_status` varchar(50) NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_header`
--

INSERT INTO `tbl_order_header` (`order_id`, `date_transac`, `customer_id`, `status`, `discount`, `loginstatus`, `transac_status`) VALUES
(37, '2008-02-15', 1, 'S', '0.00', 1, 'pending'),
(40, '2008-03-15', 17, 'S', '0.00', 1, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

CREATE TABLE IF NOT EXISTS `tbl_package` (
`package_id` int(11) NOT NULL,
  `desc` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_package`
--

INSERT INTO `tbl_package` (`package_id`, `desc`) VALUES
(1, 'Regular'),
(2, 'Family'),
(3, 'Grande'),
(4, 'Party'),
(5, 'Supreme'),
(6, 'None');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_details`
--

CREATE TABLE IF NOT EXISTS `tbl_pos_details` (
`pos_detail_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_header`
--

CREATE TABLE IF NOT EXISTS `tbl_pos_header` (
`pos_id` int(11) NOT NULL,
  `date_transac` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S',
  `discount` decimal(12,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_po_details`
--

CREATE TABLE IF NOT EXISTS `tbl_po_details` (
`po_detail_id` int(11) NOT NULL,
  `po_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_po_details`
--

INSERT INTO `tbl_po_details` (`po_detail_id`, `po_header_id`, `stock_id`, `cost`, `qty`, `amount`) VALUES
(1, 1, 12, '12.00', 2, '24.00'),
(2, 2, 12, '12.00', 2, '24.00'),
(3, 2, 11, '12.00', 5, '60.00'),
(4, 2, 10, '12.00', 122, '1464.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_po_header`
--

CREATE TABLE IF NOT EXISTS `tbl_po_header` (
`po_header_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `date_transac` date NOT NULL DEFAULT '0000-00-00',
  `status` char(1) NOT NULL DEFAULT 'S'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_po_header`
--

INSERT INTO `tbl_po_header` (`po_header_id`, `supplier_id`, `date_transac`, `status`) VALUES
(1, 12, '2015-07-01', 'C'),
(2, 12, '2015-07-01', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
`stock_id` int(10) NOT NULL,
  `stock` varchar(100) NOT NULL,
  `stock_code` varchar(100) NOT NULL,
  `category` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `package_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `price1` decimal(12,2) NOT NULL,
  `price2` decimal(12,2) NOT NULL,
  `price3` decimal(12,2) NOT NULL,
  `price4` decimal(12,2) NOT NULL,
  `price5` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`stock_id`, `stock`, `stock_code`, `category`, `supplier_id`, `type`, `package_id`, `image`, `cost`, `price1`, `price2`, `price3`, `price4`, `price5`) VALUES
(2, 'test1', '', 6, 0, 'FP', 2, 'food_icon02.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(3, 'test1', '', 6, 0, 'FP', 3, 'food_icon03.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(4, 'test1', '', 6, 0, 'FP', 4, 'food_icon04.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(5, 'test1', '', 6, 0, 'FP', 5, 'food_icon05.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(6, 'test1', '', 5, 0, 'FP', 6, 'food_icon06.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(7, 'test1', '', 2, 0, 'FP', 6, 'food_icon07.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(8, 'test1', '', 3, 0, 'FP', 6, 'food_icon01.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(9, 'desert', '', 4, 0, 'FP', 6, 'food_icon02.jpg', '0.00', '300.00', '400.00', '500.00', '600.00', '900.00'),
(10, 'vegetable1', '', 7, 12, 'RP', 6, 'food_icon03.jpg', '12.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(11, 'testvegetable2', '', 7, 12, 'RP', 6, 'food_icon04.jpg', '12.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(12, 'test1vegetable3', '', 7, 12, 'RP', 6, 'food_icon05.jpg', '12.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(13, 'Ham and Cheese', '', 6, 0, 'FP', 2, '0foodicon06.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(14, 'Egg Plant', '', 7, 1, 'RP', 6, '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservation`
--

CREATE TABLE IF NOT EXISTS `tbl_reservation` (
`reservation_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `reservation_date` date NOT NULL,
  `phone` varchar(50) NOT NULL,
  `guest_number` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rr_details`
--

CREATE TABLE IF NOT EXISTS `tbl_rr_details` (
`rr_detail_id` int(11) NOT NULL,
  `rr_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rr_details`
--

INSERT INTO `tbl_rr_details` (`rr_detail_id`, `rr_header_id`, `stock_id`, `cost`, `qty`, `amount`) VALUES
(1, 1, 12, '12.00', 2, '24.00'),
(2, 1, 11, '12.00', 3, '36.00'),
(3, 1, 10, '12.00', 100, '1200.00'),
(4, 2, 11, '12.00', 1, '12.00'),
(5, 2, 10, '12.00', 5, '60.00'),
(6, 3, 11, '12.00', 2, '24.00'),
(7, 3, 10, '12.00', 10, '120.00'),
(8, 4, 10, '12.00', 12, '144.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rr_header`
--

CREATE TABLE IF NOT EXISTS `tbl_rr_header` (
`rr_header_id` int(11) NOT NULL,
  `po_header_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transac` date NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'S'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rr_header`
--

INSERT INTO `tbl_rr_header` (`rr_header_id`, `po_header_id`, `supplier_id`, `date_transac`, `status`) VALUES
(1, 2, 12, '2015-07-30', 'S'),
(2, 2, 12, '2015-07-30', 'C'),
(3, 2, 12, '2015-04-30', 'S'),
(4, 2, 12, '2015-07-31', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE IF NOT EXISTS `tbl_supplier` (
`supplier_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`supplier_id`, `name`, `date_added`, `status`) VALUES
(1, 'Seasons', '0000-00-00 00:00:00', 'active'),
(2, 'Seasons', '0000-00-00 00:00:00', 'active'),
(3, 'Seasons', '0000-00-00 00:00:00', 'active'),
(4, 'Seasons', '0000-00-00 00:00:00', 'active'),
(5, 'Seasons', '0000-00-00 00:00:00', 'active'),
(6, 'Seasons', '0000-00-00 00:00:00', 'active'),
(7, 'Seasons', '0000-00-00 00:00:00', 'active'),
(8, 'Seasons', '0000-00-00 00:00:00', 'active'),
(9, 'Seasons', '0000-00-00 00:00:00', 'active'),
(10, 'Seasons', '0000-00-00 00:00:00', 'active'),
(11, 'Seasons', '0000-00-00 00:00:00', 'active'),
(12, 'Savemore', '2015-07-25 14:48:14', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_formula`
--

CREATE TABLE IF NOT EXISTS `tbl_temp_formula` (
`temp_formula_detail_id` int(11) NOT NULL,
  `temp_formula_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_formula_header`
--

CREATE TABLE IF NOT EXISTS `tbl_temp_formula_header` (
`temp_formulation_id` int(11) NOT NULL,
  `formulationcode` varchar(50) NOT NULL,
  `formulationdate` date NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `finishedproduct` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_po`
--

CREATE TABLE IF NOT EXISTS `tbl_temp_po` (
`temp_po_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transac` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_po_details`
--

CREATE TABLE IF NOT EXISTS `tbl_temp_po_details` (
`temp_po_detail_id` int(11) NOT NULL,
  `temp_po_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
`user_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `level` int(3) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `password`, `description`, `level`) VALUES
(1, 'admin', 'admin', 'admin', 1),
(3, 'cash123', '12345', 'cashier', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
 ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
 ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
 ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `tbl_formulation_details`
--
ALTER TABLE `tbl_formulation_details`
 ADD PRIMARY KEY (`formulationdetail_id`);

--
-- Indexes for table `tbl_formulation_header`
--
ALTER TABLE `tbl_formulation_header`
 ADD PRIMARY KEY (`formulation_id`);

--
-- Indexes for table `tbl_joborder_details`
--
ALTER TABLE `tbl_joborder_details`
 ADD PRIMARY KEY (`joborderdetail_id`);

--
-- Indexes for table `tbl_joborder_header`
--
ALTER TABLE `tbl_joborder_header`
 ADD PRIMARY KEY (`joborder_id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
 ADD PRIMARY KEY (`order_detail_id`);

--
-- Indexes for table `tbl_order_header`
--
ALTER TABLE `tbl_order_header`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_package`
--
ALTER TABLE `tbl_package`
 ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `tbl_pos_details`
--
ALTER TABLE `tbl_pos_details`
 ADD PRIMARY KEY (`pos_detail_id`);

--
-- Indexes for table `tbl_pos_header`
--
ALTER TABLE `tbl_pos_header`
 ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `tbl_po_details`
--
ALTER TABLE `tbl_po_details`
 ADD PRIMARY KEY (`po_detail_id`);

--
-- Indexes for table `tbl_po_header`
--
ALTER TABLE `tbl_po_header`
 ADD PRIMARY KEY (`po_header_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
 ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `tbl_reservation`
--
ALTER TABLE `tbl_reservation`
 ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `tbl_rr_details`
--
ALTER TABLE `tbl_rr_details`
 ADD PRIMARY KEY (`rr_detail_id`);

--
-- Indexes for table `tbl_rr_header`
--
ALTER TABLE `tbl_rr_header`
 ADD PRIMARY KEY (`rr_header_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
 ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tbl_temp_formula`
--
ALTER TABLE `tbl_temp_formula`
 ADD PRIMARY KEY (`temp_formula_detail_id`);

--
-- Indexes for table `tbl_temp_formula_header`
--
ALTER TABLE `tbl_temp_formula_header`
 ADD PRIMARY KEY (`temp_formulation_id`);

--
-- Indexes for table `tbl_temp_po`
--
ALTER TABLE `tbl_temp_po`
 ADD PRIMARY KEY (`temp_po_id`);

--
-- Indexes for table `tbl_temp_po_details`
--
ALTER TABLE `tbl_temp_po_details`
 ADD PRIMARY KEY (`temp_po_detail_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
MODIFY `cat_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
MODIFY `employee_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_formulation_details`
--
ALTER TABLE `tbl_formulation_details`
MODIFY `formulationdetail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_formulation_header`
--
ALTER TABLE `tbl_formulation_header`
MODIFY `formulation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_joborder_details`
--
ALTER TABLE `tbl_joborder_details`
MODIFY `joborderdetail_id` bigint(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_joborder_header`
--
ALTER TABLE `tbl_joborder_header`
MODIFY `joborder_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_order_header`
--
ALTER TABLE `tbl_order_header`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tbl_package`
--
ALTER TABLE `tbl_package`
MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_pos_details`
--
ALTER TABLE `tbl_pos_details`
MODIFY `pos_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_pos_header`
--
ALTER TABLE `tbl_pos_header`
MODIFY `pos_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_po_details`
--
ALTER TABLE `tbl_po_details`
MODIFY `po_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_po_header`
--
ALTER TABLE `tbl_po_header`
MODIFY `po_header_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
MODIFY `stock_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_reservation`
--
ALTER TABLE `tbl_reservation`
MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_rr_details`
--
ALTER TABLE `tbl_rr_details`
MODIFY `rr_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_rr_header`
--
ALTER TABLE `tbl_rr_header`
MODIFY `rr_header_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
MODIFY `supplier_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_temp_formula`
--
ALTER TABLE `tbl_temp_formula`
MODIFY `temp_formula_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_temp_formula_header`
--
ALTER TABLE `tbl_temp_formula_header`
MODIFY `temp_formulation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_temp_po`
--
ALTER TABLE `tbl_temp_po`
MODIFY `temp_po_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_temp_po_details`
--
ALTER TABLE `tbl_temp_po_details`
MODIFY `temp_po_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
