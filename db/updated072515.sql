-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2015 at 01:10 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_capstone_order`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
`cat_id` int(10) NOT NULL,
  `category` varchar(10) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cat_id`, `category`, `type`) VALUES
(1, 'all', 'products'),
(2, 'breakfast', 'products'),
(3, 'special', 'products'),
(4, 'desert', 'products'),
(5, 'dinner', 'products'),
(6, 'pizza', 'products');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE IF NOT EXISTS `tbl_customer` (
`customer_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_id`, `name`, `status`, `birthday`, `date_added`) VALUES
(1, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(2, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(3, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(4, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(5, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(6, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(7, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(8, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(9, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(10, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(11, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18'),
(12, 'milky', 'active', '1989-09-03', '2015-07-25 15:25:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE IF NOT EXISTS `tbl_employee` (
`employee_id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`employee_id`, `name`, `status`, `date_added`) VALUES
(1, 'Milky', 'active', '2015-07-25 16:02:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_po_details`
--

CREATE TABLE IF NOT EXISTS `tbl_po_details` (
`po_detail_id` int(11) NOT NULL,
  `po_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_po_header`
--

CREATE TABLE IF NOT EXISTS `tbl_po_header` (
`po_header_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `date_transac` date NOT NULL DEFAULT '0000-00-00',
  `status` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
`stock_id` int(10) NOT NULL,
  `stock` varchar(100) NOT NULL,
  `stock_code` varchar(100) NOT NULL,
  `category` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `price1` decimal(12,2) NOT NULL,
  `price2` decimal(12,2) NOT NULL,
  `price3` decimal(12,2) NOT NULL,
  `price4` decimal(12,2) NOT NULL,
  `price5` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`stock_id`, `stock`, `stock_code`, `category`, `supplier_id`, `image`, `cost`, `price1`, `price2`, `price3`, `price4`, `price5`) VALUES
(1, 'test1', '', 1, 0, 'food_icon01.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(2, 'test1', '', 2, 0, 'food_icon02.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(3, 'test1', '', 3, 0, 'food_icon03.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(4, 'test1', '', 4, 0, 'food_icon04.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(5, 'test1', '', 5, 0, 'food_icon05.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(6, 'test1', '', 6, 0, 'food_icon06.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(7, 'test1', '', 2, 0, 'food_icon07.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(8, 'test1', '', 3, 0, 'food_icon01.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(9, 'test1', '', 4, 0, 'food_icon02.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(10, 'test1', '', 5, 0, 'food_icon03.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(11, 'test1', '', 6, 0, 'food_icon04.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(12, 'test1', '', 7, 0, 'food_icon05.jpg', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rr_details`
--

CREATE TABLE IF NOT EXISTS `tbl_rr_details` (
`rr_detail_id` int(11) NOT NULL,
  `rr_header_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cost` decimal(12,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rr_header`
--

CREATE TABLE IF NOT EXISTS `tbl_rr_header` (
`rr_header_id` int(11) NOT NULL,
  `po_header_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transac` date NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE IF NOT EXISTS `tbl_supplier` (
`supplier_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`supplier_id`, `name`, `date_added`, `status`) VALUES
(1, 'Seasons', '0000-00-00 00:00:00', 'active'),
(2, 'Seasons', '0000-00-00 00:00:00', 'active'),
(3, 'Seasons', '0000-00-00 00:00:00', 'active'),
(4, 'Seasons', '0000-00-00 00:00:00', 'active'),
(5, 'Seasons', '0000-00-00 00:00:00', 'active'),
(6, 'Seasons', '0000-00-00 00:00:00', 'active'),
(7, 'Seasons', '0000-00-00 00:00:00', 'active'),
(8, 'Seasons', '0000-00-00 00:00:00', 'active'),
(9, 'Seasons', '0000-00-00 00:00:00', 'active'),
(10, 'Seasons', '0000-00-00 00:00:00', 'active'),
(11, 'Seasons', '0000-00-00 00:00:00', 'active'),
(12, 'Savemore', '2015-07-25 14:48:14', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
`user_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `level` int(3) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `password`, `description`, `level`) VALUES
(1, 'admin', 'admin', 'admin', 1),
(3, 'cash123', '12345', 'cashier', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
 ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
 ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `tbl_po_details`
--
ALTER TABLE `tbl_po_details`
 ADD PRIMARY KEY (`po_detail_id`);

--
-- Indexes for table `tbl_po_header`
--
ALTER TABLE `tbl_po_header`
 ADD PRIMARY KEY (`po_header_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
 ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `tbl_rr_details`
--
ALTER TABLE `tbl_rr_details`
 ADD PRIMARY KEY (`rr_detail_id`);

--
-- Indexes for table `tbl_rr_header`
--
ALTER TABLE `tbl_rr_header`
 ADD PRIMARY KEY (`rr_header_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
 ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
MODIFY `cat_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
MODIFY `employee_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_po_details`
--
ALTER TABLE `tbl_po_details`
MODIFY `po_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_po_header`
--
ALTER TABLE `tbl_po_header`
MODIFY `po_header_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
MODIFY `stock_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_rr_details`
--
ALTER TABLE `tbl_rr_details`
MODIFY `rr_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_rr_header`
--
ALTER TABLE `tbl_rr_header`
MODIFY `rr_header_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
MODIFY `supplier_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
