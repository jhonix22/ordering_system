<?php

/**
 * File to handle all API requests
 * Accepts GET and POST
 * 
 * Each request will be identified by TAG
 * Response will be JSON data
*/
/**
 * check for POST request 
 */
#$_POST['tag'] = "price";
#$_POST['name'] = "test1";
#$_POST['password'] = "12345";
if (isset($_POST['tag']) && $_POST['tag'] != '') {
    // get tag
    $tag = $_POST['tag'];

    // include db handler
    require_once '../admin/library/config.php';

    // response Array
    $response = array("tag" => $tag, "success" => 0, "error" => 0);

    // check for tag type
    if ($tag == 'login') {
        // Request type is check Login
        $email = $_POST['email'];
        $password = $_POST['password'];

        // check for user
        $sql=mysql_query("SELECT * FROM tbl_user WHERE username = '$email' AND password = '$password'");

        if (mysql_num_rows($sql)) {
            // user found
            // echo json with success = 1
            $user = mysql_fetch_assoc($sql);

            $response["success"] = 1;
            $response["uid"] = $user["user_id"];
            $response["user"]["name"] = $user["username"];
            $response["user"]["email"] = $user["description"];
            echo json_encode($response);

        } else {
            // user not found
            // echo json with error = 1
            $response["error"] = 1;
            $response["error_msg"] = "Incorrect email or password!";
            echo json_encode($response);
        }
    } else if ($tag == 'register') {
        // Request type is Register new user
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        // check if user is already existed
        if ($db->isUserExisted($email)) {
            // user is already existed - error response
            $response["error"] = 2;
            $response["error_msg"] = "User already existed";
            echo json_encode($response);
        } else {
            // store user
            $user = $db->storeUser($name, $email, $password);
            if ($user) {
                // user stored successfully
                $response["success"] = 1;
                $response["uid"] = $user["unique_id"];
                $response["user"]["name"] = $user["name"];
                $response["user"]["email"] = $user["email"];
                $response["user"]["created_at"] = $user["created_at"];
                $response["user"]["updated_at"] = $user["updated_at"];
                echo json_encode($response);
            } else {
                // user failed to store
                $response["error"] = 1;
                $response["error_msg"] = "Error occured in Registartion";
                echo json_encode($response);
            }
        }
    }else if($tag=="getproducts"){
				$s=mysql_query("SELECT * FROM tbl_product WHERE category = '".$_POST['categ']."' order by stock asc");
				$i=0;
				while($r=mysql_fetch_assoc($s)){
					$response["products"]['id'][$i] = $r['stock_id'];
					$response["products"]['name'][$i] = $r['stock'];
					$response["products"]['image'][$i] = $r['image'];
					$response["products"]['description'][$i] = $r['description'];
					$response["products"]['price'][$i] = $r['price1'];
					$i++;
				}
				$response['success'] = 1;
				echo json_encode($response);
				#var_dump($response);
	}else if($tag == "price"){
				#echo "SELECT * FROM tbl_product WHERE stock = '".$_POST['name']."'";
				$s=mysql_query("SELECT * FROM tbl_product WHERE stock = '".$_POST['name']."'");
				$i=0;
				while($r=mysql_fetch_assoc($s)){
					$response["price"][$i] = $r['price1'];
					$i++;
				}
				$response['success'] = 1;
				echo json_encode($response);
				#var_dump($response);
	}else if($tag=="insert_trans"){
				$w=mysql_query("SELECT * FROM tbl_user WHERE username = '".$_POST['name']."'");
				$rr=mysql_fetch_assoc($w);
				$user_id = $rr['user_id'];
		
				$s=mysql_query("INSERT INTO tbl_pos_header(date_transac,customer_name,user_id) VALUES (NOW(),'".$_POST['remarks']."','".$user_id."')");
				if($s){
					$response['id'] = mysql_insert_id();
				}
				$response['success'] = 1;
				echo json_encode($response);
	}else if($tag=="insert"){
				$sql=mysql_query("SELECT * FROM tbl_product WHERE stock = '".$_POST['product']."'");
				$rr=mysql_fetch_assoc($sql);
				$amount = $_POST['price'] * $_POST['qty'];
				
				$s=mysql_query("INSERT INTO tbl_pos_details(pos_id,stock_id,qty,package_id,price,amount)
								VALUES 
								('".$_POST['transaction_id']."','".$rr['stock_id']."','".$_POST['qty']."','".$rr['package_id']."','".$_POST['price']."','".$amount."')");
				if($s){
					$response['id'] = mysql_insert_id();
				}
				$response['success'] = 1;
				echo json_encode($response);
	}else if($tag == "price_package"){
				#echo "SELECT * FROM tbl_product WHERE stock = '".$_POST['name']."'";
				$s=mysql_query("SELECT * FROM tbl_product WHERE stock = '".$_POST['name']."'");
				$i=0;
				$r=mysql_fetch_assoc($s);
				switch($_POST['size']){
					case 'Regular':
						$response["price"] = $r['price1'];
					break;
					
					case 'Family':
						$response["price"] = $r['price2'];
					break;
					
					case 'Grande':
						$response["price"] = $r['price3'];
					break;
					
					case 'Party':
						$response["price"] = $r['price4'];
					break;
					
					case 'Supreme':
						$response["price"] = $r['price5'];
					break;
					
				}
				/*while($r=mysql_fetch_assoc($s)){
					$response["price"][$i] = $r['price1'];
					$i++;
				}*/
				$response['success'] = 1;
				echo json_encode($response);
				#var_dump($response);
	}else if($tag == "package"){
				#echo "SELECT * FROM tbl_product WHERE stock = '".$_POST['name']."'";
				$s=mysql_query("SELECT * FROM tbl_product WHERE stock = '".$_POST['name']."'");
				$i=0;
				$r=mysql_fetch_assoc($s);
				$ss=mysql_query("SELECT * FROM tbl_category WHERE cat_id = '".$r['category']."'");
				$rr=mysql_fetch_assoc($ss);
				$response['categ'] = $rr['category'];
				$response['success'] = 1;
				echo json_encode($response);
	}else {
        echo "Invalid Request";
    }
} else {
    echo "Access Denied";
}
?>

